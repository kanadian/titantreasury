-- **************************************************************************
-- * TitanTreasuryRightClick.lua
-- *
-- * By: KanadiaN (Lindarena@Laughing Skull)
-- *
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanTreasury", true );

-- **************************************************************************
-- NAME : TitanPanelRightClickMenu_PrepareTreasuryMenu()
-- DESC : Builds Right Click Interactive Menu
-- **************************************************************************
function TitanPanelRightClickMenu_PrepareTreasuryMenu()
     local cRealm = TitanTreasury_ColorUtils( "GetCFaction", TitanTreasuryProfile.Faction, TitanTreasuryProfile.Realm );
     local cDelete = TitanTreasury_ColorUtils( "GetCText", "RED", LB["TITAN_TREASURY_MENU_DELETE"] );
     local devUser = ( "%s@%s" ):format( TitanTreasuryProfile.Player, TitanTreasuryProfile.Realm )
     local author = TitanTreasury_CoreUtils( "Author", devUser );
     if ( TitanPanelRightClickMenu_GetDropdownLevel() == 2 ) then 
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == cRealm ) then
               -- Toon Data Delete
               info = {};
               info.notCheckable = true;
               info.text = cDelete;
               info.hasArrow = 1;
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               -- Toon Tooltip Hide
               info = {};
               info.notCheckable = true;
               info.text = LB["TITAN_TREASURY_MENU_HIDETOON"];
               info.hasArrow = 1;
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
          end
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == "Debugging" ) then
               info = {};
               info.checked = TitanTreasuryDebug.State;
               info.text = "State";
               info.value = "State";
               info.func = function() TitanTreasury_DebugUtils( "Toggle", "State" ); return; end
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               if ( author and TitanTreasuryDebug.State ) then
                    info = {};
                    info.notCheckable = true;
                    info.text = "Tooltip";
                    info.value = "Tooltip";
                    info.func = function() TitanPanelTreasuryButton_GetTooltipText(); end
                    TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                    info = {};
                    info.notCheckable = true;
                    info.text = "Logging";
                    info.hasArrow = true;
                    TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               end
          end
          return;
     end
     if ( TitanPanelRightClickMenu_GetDropdownLevel() == 3 ) then
	  if ( TitanPanelRightClickMenu_GetDropdMenuValue() == cDelete ) then
               -- Delete Toon Data
               local dTable = TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ];
               table.foreach( dTable, function( Key, Data )
                         if ( Key ~= UnitName( "player" ) ) then
                              info = {};
                              info.notCheckable = true;
                              info.text = TitanTreasury_ColorUtils( "GetCClass", Data.Class, Key );
                              info.func = function()
                                   TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ Key ] = nil;
                              end;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         end
                    end
               );
               TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
               -- Clears All Data
               info = {};
               info.notCheckable = true;
               info.text = LB["TITAN_TREASURY_MENU_CLEARALL"];
               info.func = function()
                    TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ] = {};
               end;
               info.value = LB["TITAN_TREASURY_MENU_CLEARALL"];
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
          end
          -- Hide Toon From Tooltip
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == LB["TITAN_TREASURY_MENU_HIDETOON"] ) then
               local dTable = TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ];
               local tHide = 0;
               table.foreach( dTable, function( Key, Data )
                         if ( Key ~= UnitName( "player" ) ) then
                              info = {};
                              info.notCheckable = true;
                              info.text = TitanTreasury_ColorUtils( "GetCClass", Data.Class, Key );
                              info.func = function()
                                   TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ].Hide = not Data.Hide;
                                   TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                              end;
                              -- info.checked = Data.Hide;
                              TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                              if ( Data.Hide ) then tHide = tHide + 1; end
                         end
                    end
               );
               -- Show All Toons
               if ( tHide > 0 ) then
                    TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
                    info = {};
                    info.notCheckable = true;
                    info.text = LB["TITAN_TREASURY_MENU_SHOWALL"];
                    info.value = LB["TITAN_TREASURY_MENU_SHOWALL"];
                    info.func = function()
                         local dTable = TitanTreasuryCharacters[ TitanTreasuryProfile.Faction ][ TitanTreasuryProfile.Realm ];
                         table.foreach( dTable, function( Key, Data )
                                   if ( Data.Hide ) then
                                        TitanTreasuryCharacters[ TitanTreasuryProfile.Faction ][ TitanTreasuryProfile.Realm ][ Key ].Hide = not Data.Hide;
                                   end;
                              end
                         );
                         TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                    end;
                    TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               end
          end
          -- For debugging
          if ( TitanPanelRightClickMenu_GetDropdMenuValue() == "Logging" ) then
               info = {};
               info.checked = TitanTreasuryDebug.LogEvent;
               info.text = "Log Events";
               info.value = "Log Evets";
               info.func = function() TitanTreasury_DebugUtils( "Toggle", "LogEvent" ); end
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
               info = {};
               info.notCheckable = true;
               info.text = "Clear Event Log";
               info.value = "Clear Event Log";
               info.func = function() TitanTreasuryDebug.Log = {}; end
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
               info = {};
               info.notCheckable = true;
               info.text = "Play Event Log";
               info.value = "Playe Event Log";
               info.func = function() TitanTreasury_ArrayUtils( "playData", TitanTreasuryDebug.Log, true ); end
               TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
          end
	  return;
     end
     -- Tooltip Menu Title
     TitanPanelRightClickMenu_AddTitle( TitanPlugins[ LB["TITAN_TREASURY_CORE_ID"] ].menuText );
     -- Realm Menu
     info = {};
     info.notCheckable = true;
     info.text = cRealm;
     info.hasArrow = 1;
     TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
     TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
     -- Settings MEnu
     info = {};
     info.notCheckable = true;
     info.text = LB["TITAN_TREASURY_MENU_SETTINGS"];
     info.value = LB["TITAN_TREASURY_MENU_SETTINGS"];
     info.func = function() TitanTreasury_InitGUI(); end
     TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
     TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
     if ( author ) then
          info = {};
          info.notCheckable = true;
          info.hasArrow = true;
          info.text = "Debugging";
          TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
          TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
     end
     -- Standard Menu
     TitanPanelRightClickMenu_AddControlVars( LB["TITAN_TREASURY_CORE_ID"] );
     -- Custom Menu
     TitanPanelRightClickMenu_AddSpacer( TitanPanelRightClickMenu_GetDropdownLevel() );
     TitanPanelRightClickMenu_AddCommand( LB["TITAN_TREASURY_MENU_CLOSE"], LB["TITAN_TREASURY_CORE_ID"], function() return; end );
end