-- **************************************************************************
--   TitanTreasuryUtils.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanTreasury", true );
local TitanTreasuryTimer = LibStub( "AceAddon-3.0" ):NewAddon( "TitanTreasury", "AceTimer-3.0" );
local TitanTreasurySessionTimer = nil;

-- **************************************************************************
-- NAME : TitanTreasury_CoreUtils( b1, b2, b3, b4, b5 )
-- DESC : This makes everything happen
-- **************************************************************************
function TitanTreasury_CoreUtils( b1, b2, b3, b4, b5, b6 )
     local CP = TitanTreasuryProfile
     local b = { [1] = b1, [2] = b2, [3] = b3, [4] = b4, [5] = b5, [6] = b6 };
     if ( TitanTreasuryInit and TitanTreasuryDebug.States ) then
          local tdLine
          for i = 1, 10 do
               if ( i == 1 ) then
                    if ( b[i] ) then tdLine = ( "[|cffff9900%s|r" ):format( b[i] ); end
               else
                    if ( b[i] ) then tdLine = ( "%s[|cffff9900%s|r" ):format( tdLine, b[i] ); end
               end
          end
          TitanTreasuryDebug.dLine = ( "Recieved : %s" ):format( tdLine );
	  TitanTreasury_DebugUtils( "bug", "dLine", TitanTreasuryDebug.dLine );
     end
     if ( b[1] == "ReSetDB" ) then
          TitanTreasury = nil;
	  ReloadUI();
     elseif ( b[1] == "HexToDec" ) then
          return tonumber( string.sub( b[2], 1, 2 ), 16 ), tonumber( string.sub( b[2], 3, 4 ), 16), tonumber( string.sub( b[2], 5, 6 ), 16 );
     elseif ( b[1] == "HexToDecP" ) then
          local r, g, b = tonumber( string.sub( b[2], 1, 2 ), 16 ), tonumber( string.sub( b[2], 3, 4 ), 16), tonumber( string.sub( b[2], 5, 6 ), 16 );
          return r / 255, g /255, b / 255
     elseif ( b[1] == "DisplayB" ) then
          UpdateAddOnMemoryUsage();
          TitanTreasuryDebug.dLine = ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "TitanTreasury" ) ) );
          TitanTreasury_DebugUtils( "Info", "load", 43, LB["TITAN_TREASURY_CONFIG_BANNOR_FORMAT"]:format( LB["TITAN_TREASURY_CONFIG"], LB["TITAN_TREASURY_CORE_VERSION"], TitanTreasuryDebug.dLine ) );
     elseif ( b[1] == "MakeSafe" ) then
          if ( b[2] == true ) then return "true";
          elseif ( b[2] == false ) then return "false";
          elseif ( b[2] == nil ) then return "nil";
          else return b[2]; end
     elseif( b[1] == "GetMoney" ) then
          local money = b[2];
          local gold = floor( money / 10000 );
          local silver = floor( ( money - gold * 10000 ) / 100 );
          local copper = mod( money, 100 );
          local g, s, c, totalStr, mStyle, mType;
          if ( b[3] == "Button" ) then
               mStyle = TitanTreasury.Button.Style;
               mType = TitanTreasury.Button.Type;
          elseif ( b[3] == "Tooltip" ) then
               mStyle = TitanTreasury.Coins.Style;
               mType = TitanTreasury.Coins.Type;
          end
          if ( gold > 0 ) then
               g = LB[( "TITAN_TREASURY_GOLD_%s_%s" ):format( mType, mStyle )]:format( gold, 0, 0 );
               s = LB[( "TITAN_TREASURY_SILVER_%s_%s" ):format( mType, mStyle )]:format( silver, 0, 0 );
               c = LB[( "TITAN_TREASURY_COPPER_%s_%s" ):format( mType, mStyle )]:format( copper, 0, 0 );
               totalStr = ( "%s %s %s" ):format( g, s, c );
          elseif ( silver > 0 ) then
               s = LB[( "TITAN_TREASURY_SILVER_1_%s_%s" ):format( mType, mStyle )]:format( silver, 0, 0 );
               c = LB[( "TITAN_TREASURY_COPPER_%s_%s" ):format( mType, mStyle )]:format( copper, 0, 0 );
               totalStr = ( "%s %s" ):format( s, c );
          else
               c = LB[( "TITAN_TREASURY_COPPER_1_%s_%s" ):format( mType, mStyle )]:format( copper, 0, 0 );
               totalStr = ( "%s" ):format( c );
          end
          return totalStr;
     elseif ( b[1] == "Author" ) then
           local Authors = {
               ["Lindarena@Laughing Skull"] = true,
               ["St�bs�lot@Laughing Skull"] = true,
               ["Hilyna@Laughing Skull"] = true,
               ["Nagdand@Laughing Skull"] = true,
               ["Holycurves@Laughing Skull"] = true,
               ["Holytree@Laughing Skull"] = true,
               ["Acatra@Laughing Skull"] = true,
               ["Wenling@Laughing Skull"] = true,
               ["Loansone@Laughing Skull"] = true,
               ["Koton@Laughing Skull"] = true,
          }
          for i = 1, getn( TitanTreasury.AddAuth ) do
               if ( not Authors[TitanTreasury.AddAuth[i]] ) then
                    Authors[TitanTreasury.AddAuth[i]] = true;
               else
                    tremove( TitanTreasury.AddAuth, i );
               end
          end
          return Authors[b[2]];
     elseif ( b[1] == "AddAuth" or b[1] == "addauth" ) then
          b[2] = ( "%s@%s" ):format( CP.Player, CP.Realm );
          TitanTreasury_DebugUtils( "Info", "CoreUtils", 99, LB["TITAN_TREASURY_CORE_ADDAUTHOR"]:format( b[2] ) );
          tinsert( TitanTreasury.AddAuth, b[2] );
     elseif ( b[1] == "RemAuth" or b[1] == "remauth" ) then
          b[2] = ( "%s@%s" ):format( CP.Player, CP.Realm );
          for i = 1, getn( TitanTreasury.AddAuth ) do
               if ( b[2] == TitanTreasury.AddAuth[i] ) then
                    TitanTreasury_DebugUtils( "Info", "CoreUtils", 105, LB["TITAN_TREASURY_CORE_REMAUTHOR"]:format( b[2] ) );
                    tremove( TitanTreasury.AddAuth, i );
               end
          end
     elseif ( b[1] =="sortTable" ) then
          local sortBy, sortMethod, mTable
          if ( b[3] ) then
               sortBy = b[3];
          else
               sortBy = TitanTreasury.Sort.sType;
          end
          if ( b[4] ) then
               sortMethod = b[4];
          else
               sortMethod = TitanTreasury.Sort.sMethod;
          end
          mTable = b[2];
          if ( sortBy == LB["TITAN_TREASURY_CONFIG_NAME"] ) then
               if ( sortMethod == LB["TITAN_TREASURY_CONFIG_ASCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Name < key2.Name end ); 
               elseif ( sortMethod == LB["TITAN_TREASURY_CONFIG_DESCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Name > key2.Name end ); 
               end
          elseif ( sortBy == LB["TITAN_TREASURY_CONFIG_GOLD"] ) then
               if ( sortMethod == LB["TITAN_TREASURY_CONFIG_ASCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Gold < key2.Gold end );
               elseif ( sortMethod == LB["TITAN_TREASURY_CONFIG_DESCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Gold > key2.Gold end ); 
               end
          elseif ( sortBy == LB["TITAN_TREASURY_CONFIG_LEVEL"] ) then
               if ( sortMethod == LB["TITAN_TREASURY_CONFIG_ASCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Level < key2.Level end );
               elseif ( sortMethod == LB["TITAN_TREASURY_CONFIG_DESCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Level > key2.Level end ); 
               end
          elseif ( sortBy == LB["TITAN_TREASURY_CONFIG_CLASS"] ) then
               if ( sortMethod == LB["TITAN_TREASURY_CONFIG_ASCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Class < key2.Class end );
               elseif ( sortMethod == LB["TITAN_TREASURY_CONFIG_DESCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Class > key2.Class end ); 
               end
          elseif ( sortBy == LB["TITAN_TREASURY_CONFIG_FACTION"] ) then
               if ( sortMethod == LB["TITAN_TREASURY_CONFIG_ASCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Faction < key2.Faction end );
               elseif ( sortMethod == LB["TITAN_TREASURY_CONFIG_DESCENDING"] ) then
                    table.sort( mTable, function ( key1, key2 ) return key1.Faction > key2.Faction end ); 
               end
          end
          return mTable;
     elseif ( b[1] == "setUser" ) then
          local Faction, Realm, Player, Level, Class, Race = UnitFactionGroup( "player" ), GetRealmName(), UnitName( "player" ), UnitLevel( "player" ), UnitClass( "player" ), select( 2, UnitRace( "player" ) );
          if ( not TitanTreasuryProfile or TitanTreasuryProfile == nil ) then TitanTreasuryProfile = {}; end
          TitanTreasuryProfile.Faction = Faction;
          TitanTreasuryProfile.Realm = Realm;
          TitanTreasuryProfile.Player = Player;
          if ( not TitanTreasuryCharacters ) then TitanTreasuryCharacters = {}; end
          if ( not TitanTreasuryCharacters[ Realm ] ) then TitanTreasuryCharacters[ Realm ] = {}; end
          if ( not TitanTreasuryCharacters[ Realm ][ Player ] ) then TitanTreasuryCharacters[ Realm ][ Player ] = {}; end
          if ( not TitanTreasuryCharacters[ Realm ][ Player ].Hide or TitanTreasuryCharacters[ Realm ][ Player ].Hide == nil ) then TitanTreasuryCharacters[ Realm ][ Player ].Hide = false; end
          if ( not TitanTreasuryCharacters[ Realm ][ Player ].Tokens or TitanTreasuryCharacters[ Realm ][ Player ].Tokens == nil ) then TitanTreasuryCharacters[ Realm ][ Player ].Tokens = {}; end
          if ( not TitanTreasuryCharacters[ Realm ][ Player ].Professions or TitanTreasuryCharacters[ Realm ][ Player ].Professions == nil ) then TitanTreasuryCharacters[ Realm ][ Player ].Professions = {}; end
          if ( not TitanTreasuryCharacters[ Realm ][ Player ].Played or TitanTreasuryCharacters[ Realm ][ Player ].Played == nil ) then TitanTreasuryCharacters[ Realm ][ Player ].Played = {}; end
          TitanTreasuryCharacters[ Realm ][ Player ].Level = UnitLevel( "player" );
          TitanTreasuryCharacters[ Realm ][ Player ].Class = UnitClass( "player" );
          TitanTreasuryCharacters[ Realm ][ Player ].Gold = GetMoney();
          TitanTreasuryCharacters[ Realm ][ Player ].rGold = GetMoney();
          TitanTreasuryCharacters[ Realm ][ Player ].Session = GetTime();
          TitanTreasuryCharacters[ Realm ][ Player ].Faction = Faction;
          TitanTreasuryCharacters[ Realm ][ Player ].sRep = 0;
          if ( Faction ~= "Neutral" ) then
               TitanTreasuryCharacters[ Realm ][ Player ].Badge = Faction;
          else
               TitanTreasuryCharacters[ Realm ][ Player ].Badge = "FFA"; -- FFA FreeForAll
          end
          if ( TitanTreasuryLocals ) then
               TitanTreasuryLocals = {};
               if ( not TitanTreasuryLocals.ColorClass or TitanTreasuryLocals.ColorClass == nil ) then TitanTreasuryLocals.ColorClass = {}; end
               if ( not TitanTreasuryLocals.LocalClass or TitanTreasuryLocals.LocalClass == nil ) then TitanTreasuryLocals.LocalClass = {}; end
               if ( not TitanTreasuryLocals.ToEnglish or TitanTreasuryLocals.ToEnglish == nil ) then TitanTreasuryLocals.ToEnglish = {}; end
          end
          TitanTreasury_CoreUtils( "getLocals" );
          TitanTreasury_BuildTokensPanel();
          TitanTreasury_BuildProfessionsPanel();
          TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
          TitanTreasury_CoreUtils( "UpdateSession" );
          TitanTreasury_CoreUtils( "getRealmList" );
          TitanTreasury_ColorUtils( "GenCGradient" );
          TitanPanelButton_UpdateTooltip();
     elseif ( b[1] == "setTimePlayed" ) then
          local Faction, Realm, Player = UnitFactionGroup( "player" ), GetRealmName(), UnitName( "player" );
          TitanTreasuryCharacters[ Realm ][ Player ].Played.total = b[2];
          TitanTreasuryCharacters[ Realm ][ Player ].Played.level = b[3];
     elseif ( b[1] == "UpdateSession" ) then
          if ( b[2] == LB["TITAN_TREASURY_GUILD"] and b[3] ) then
               local sRep = TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ][ "sRep" ]; -- Update factions gain here
               TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ][ "sRep" ] = sRep + b[3];
          end
     elseif ( b[1] == "userLeveled" ) then
          local Faction, Realm, Player = UnitFactionGroup( "player" ), GetRealmName(), UnitName( "player" );
          TitanTreasuryProfile[ "Level" ] = b[2];
          TitanTreasuryCharacters[ Realm ][ Player ]["Level"] = b[2];
          if ( Faction ~= "Neutral" and TitanTreasuryCharacters[ Realm ][ Player ] ) then
               TitanTreasuryCharacters[ Realm ][ Player ][ "Faction" ] = Faction;
          end
     elseif ( b[1] == "getCount" ) then
          local count = 0
          table.foreach( b[2], function( Key, Data )
                    count = count + 1;
               end
          );
          return count
     elseif ( b[1] == "getLocals" ) then
          -- Adventurer ( hopefully a temporary fix )
          local bugFix = {
               ["TRUE"] = {
                    ["enUS"] = "Adventurer",
                    ["koKR"] = "모험가",
                    ["frFR"] = "Aventurière",
                    ["deDE"] = "Abenteurerin",
                    ["zhCN"] = "冒险家",
                    ["esES"] = "Aventurera",
                    ["zhTW"] = "冒險家",
                    ["esMX"] = "Aventurera",
                    ["ruRU"] = "Авантюрист",
                    ["ptBR"] = "Aventureira",
                    ["itIT"] = "Avventuriera",
               },
               ["FALSE"] = {
                    ["enUS"] = "Adventurer",
                    ["koKR"] = "모험가",
                    ["frFR"] = "Aventurier",
                    ["deDE"] = "Abenteurer",
                    ["zhCN"] = "冒险家",
                    ["esES"] = "Aventurero",
                    ["zhTW"] = "冒險家",
                    ["esMX"] = "Aventurero",
                    ["ruRU"] = "Авантюрист",
                    ["ptBR"] = "Aventureiro",
                    ["itIT"] = "Avventuriero",
               },
          };
          local Gender, Sex, localClass = "", "", {};
          if ( UnitSex( "player" ) == 2 ) then
               Sex = "FALSE";
          else
               Sex = "TRUE";
          end
          local Genders = { { true, "TRUE", }, { false, "FALSE", }, };
          table.foreach( Genders, function( key, data )
                    local classFix = bugFix[ data[ 2 ] ][ GetLocale() ];
                    table.foreach( FillLocalizedClassList( localClass, data[ 1 ] ), function( English, Local )
                              if ( English ~= classFix ) then
                                   local color = C_ClassColor.GetClassColor( English );
                                   color:GenerateHexColor();
                                   Gender = ( "%s_%s" ):format( English, Sex );
                                   TitanTreasuryLocals.ColorClass[Gender] = color:WrapTextInColorCode("%s");
                                   TitanTreasuryLocals.LocalClass[Gender] = Local;
                                   TitanTreasuryLocals.ToEnglish[Local] = Gender;
                              end
                         end
                    );
               end
          );
          TitanTreasuryProfile.Locale = GetLocale();
     elseif ( b[1] == "tooltipTimer" ) then
          if ( b[2] == "start" ) then 
               if ( TitanTreasurySessionTimer == nil or not TitanTreasurySessionTimer and TitanTreasury.Sessions.ShowSession and TitanTreasury.Sessions.ShowHourly and TitanTreasury.General.tooltipTimer ) then
                    TitanTreasurySessionTimer = TitanTreasuryTimer:ScheduleRepeatingTimer( TitanPanelPluginHandle_OnUpdate, TitanTreasury.General.timerInterval, { LB["TITAN_TREASURY_CORE_ID"], 2 } );
               end
          elseif ( b[2] == "stop" ) then
               if ( TitanTreasurySessionTimer ) then
                    TitanTreasuryTimer:CancelTimer( TitanTreasurySessionTimer );
                    TitanTreasurySessionTimer = nil;
               end
          elseif ( b[2] == "restart" ) then 
               if ( TitanTreasurySessionTimer ) then
                    TitanTreasuryTimer:CancelTimer( TitanTreasurySessionTimer );
                    TitanTreasurySessionTimer = TitanTreasuryTimer:ScheduleRepeatingTimer( TitanPanelPluginHandle_OnUpdate, TitanTreasury.General.timerInterval, { LB["TITAN_TREASURY_CORE_ID"], 2 } );
               end
          end
     elseif (  b[1] == "getBadge" ) then
          local bText, image = "", ""
          local iDB = {
               "|TInterface\\TargetingFrame\\UI-PVP-%s:16:16:0:0:64:64:0:40:0:40|t",
               "|TInterface\\TargetingFrame\\UI-PVP-%s:16:16:0:0:64:64:0:40:0:40|t %s",
          }
          if ( not b[3] ) then b[3] = b[2]; end
          if ( b[2] == "Neutral" ) then b[2] = "FFA" end
          if ( TitanTreasury.General.ShowBadgeText ) then 
               if ( TitanTreasury.General.ShowBadgeColor ) then
                    bText = TitanTreasury_ColorUtils( "GetCFaction", b[3], b[3] )
               else
                    bText = b[3];
               end
          end
          if ( TitanTreasury.General.ShowBadge ) then
               if ( TitanTreasury.General.ShowBadgeText ) then
                    image = iDB[2]:format( b[2], bText );
               else
                    image = iDB[1]:format( b[2] );
               end
          else
               image = bText;
          end
          return image;
     elseif ( b[1] == "getTotal" ) then
          local money = 0;
          table.foreach( b[2], function( faction, value )
                    money = money + value;
               end
          );
          return money
     elseif ( b[1] == "getImage" ) then
          return b[2], b[3], b[3];
     elseif ( b[1] == "getTradeSkillIndex" ) then
          local index
          if ( b[2] == 75 ) then index = 2;
          elseif ( b[2] == 150 ) then index = 3;
          elseif ( b[2] == 225 ) then index = 4;
          elseif ( b[2] == 300 ) then index = 5;
          elseif ( b[2] == 375 ) then index = 6;
          elseif ( b[2] == 450 ) then index = 7;
          elseif ( b[2] == 525 ) then index = 8;
          elseif ( b[2] == 600 ) then index = 9;
          end
          return index
     elseif ( b[1] == "getMoneyTable" ) then
          local TP, aData, mTable = "", {}, {};
          TP = TitanTreasuryProfile;
          aData = TitanTreasuryCharacters[ TP.Realm ];
          table.foreach( aData, function( Key, Data )
                    if ( Key == b[3].Player ) then
                         table.insert( mTable, { Name = Key, Class = Data.Class, Gold = Data.Gold, rGold = Data.rGold, Level = Data.Level, Faction = Data.Faction, Badge = Data.Badge } );
                    elseif ( Key ~= b[3].Player and TitanTreasury.General.ShowAlts and not Data.Hide ) then
                         if ( Data.Faction == b[3].Faction ) then 
                             table.insert( mTable, { Name = Key, Class = Data.Class, Gold = Data.Gold, Level = Data.Level, Faction = Data.Faction, Badge = Data.Badge } );
                         elseif ( TitanTreasury.General.ShowAll and Data.Faction ~= b[3].Faction or b[4] ) then
                             table.insert( mTable, { Name = Key, Class = Data.Class, Gold = Data.Gold, Level = Data.Level, Faction = Data.Faction, Badge = Data.Badge } );
                         end
                    end
               end
          );
          mTable = TitanTreasury_CoreUtils( "sortTable", mTable );
          return mTable;
     elseif ( b[1] == "getAccountTable" ) then
          local rTotal, aData, aTable = 0, {}, {};
          aData = TitanTreasuryCharacters;
          table.foreach( aData, function( Realm, Data )
                    rTotal = 0;
                    table.foreach( Data, function( Toon, Info )
                              if (  Info.Faction == b[3].Faction ) then
                                   rTotal = rTotal + Info.Gold;
                              elseif ( TitanTreasury.General.ShowAll and Info.Faction ~= b[3].Faction or b[4] ) then
                                   rTotal = rTotal + Info.Gold;
                              end
                         end
                    )
                    table.insert( aTable, { Name = Realm, Gold = rTotal } );
               end
          );
          aTable = TitanTreasury_CoreUtils( "sortTable", aTable );
          return aTable;
     elseif ( b[1] == "getPlayedTable" ) then
          local  pTotal, rTotal, cData, aTable, rTable, rRealm = 0, {}, {}, {}, {}, {};
          cData = TitanTreasuryCharacters;
          table.foreach( cData, function( Realm, Data )
                    if ( not rTotal[ Realm ] or rTotal[ Realm ] == nil ) then rTotal[ Realm ] = {}; end
                    pTotal = 0;
                    table.foreach( Data, function( Toon, Info )
                              if ( Info.Played and Info.Played.total ) then
                                   pTotal = pTotal + Info.Played.total;
                                   rTotal[ Realm ][ Toon ] = Info.Played.total;
                              end
                         end
                    )
                    rRealm[ Realm ] = pTotal;
               end
          );
          if ( b[2] == "watched" ) then
               rTable = rTotal[ b[3] ];
          elseif ( b[2] == "summary" ) then
               rTable = rRealm;
          end
          return rTable;
     elseif ( b[1] == "formatPlayed" ) then
          local nDays, days, nHours, hours, nMinutes, minutes, nSeconds, seconds
          local rStr = "";
          nDays = math.floor( b[2] / 86400 );
          nHours = math.floor( ( bit.mod( b[2],86400 ) ) / 3600 );
          nMinutes = math.floor( bit.mod( (bit.mod( b[2],86400 ) ), 3600 )/60);
          nSeconds = math.floor( bit.mod( bit.mod( ( bit.mod( b[2], 86400 ) ), 3600 ), 60 ) );
          if ( nDays ~= 0 ) then
               days = LB[ ( "TITAN_TREASURY_TOOLTIP_DAYS_%d" ):format( TitanTreasury.General.days ) ]:format( TitanTreasury.Colors.Numbers, nDays, TitanTreasury.Colors.Days );
               rStr = ( "%s %s" ):format( rStr, days );
          end
          if ( nHours ~= 0 ) then
               hours = LB[ ( "TITAN_TREASURY_TOOLTIP_HOURS_%d" ):format( TitanTreasury.General.hours ) ]:format( TitanTreasury.Colors.Numbers, nHours, TitanTreasury.Colors.Hours );
               rStr = ( "%s %s" ):format( rStr, hours );
          end
          if ( nMinutes ~= 0 ) then
               minutes = LB[ ( "TITAN_TREASURY_TOOLTIP_MINUTES_%d" ):format( TitanTreasury.General.minutes ) ]:format( TitanTreasury.Colors.Numbers, nMinutes, TitanTreasury.Colors.Minutes );
               rStr = ( "%s %s" ):format( rStr, minutes );
          end
          if ( nSeconds ~= 0 ) then
               seconds = LB[ ( "TITAN_TREASURY_TOOLTIP_SECONDS_%d" ):format( TitanTreasury.General.seconds ) ]:format( TitanTreasury.Colors.Numbers, nSeconds, TitanTreasury.Colors.Seconds );
               rStr = ( "%s %s" ):format( rStr, seconds );
          end
          if ( b[2] == 0 ) then rStr = "Please Log into Character(s)"; end
          return rStr;
     elseif ( b[1] == "convertBool" ) then
          local converted;
          if ( b[2] ) then
               converted = "|cff00ff00Enabled|r|cff98afc7";
          elseif ( not b[2] ) then
               converted = "|cffff0000Disabled|r|cff98afc7";
          end
          return converted;
     elseif ( b[1] == "getRealmList" ) then
          local cData = TitanTreasuryCharacters;
          if ( not TitanTreasury.General.Watched or TitanTreasury.General == nil ) then TitanTreasury.General.Watched = 1; end
          if ( type( TitanTreasuryStaticArrays.realms ) == table ) then
               table.wipe( TitanTreasuryStaticArrays.realms );
          else
               TitanTreasuryStaticArrays.realms = {};
          end
          table.foreach( cData, function( Realm, Data )
                    table.insert( TitanTreasuryStaticArrays.realms, Realm );
               end
          )
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_ArrayUtils( a1, a2, a3, a4, a5 )
-- DESC : This makes everything happen
-- **************************************************************************
function TitanTreasury_ArrayUtils( a1, a2, a3, a4, a5 )
     local a = { [1] = a1, [2] = a2, [3] = a3, [4] = a4, [5] = a5, };
     if ( a[1] == "convertDB" ) then
          local tbl = {};
          local dbs = { "Horde", "Alliance", "Neutral" };
          table.foreach( dbs, function( _, Faction )
                    if ( TitanTreasuryCharacters[ Faction ] ) then
                         table.foreach( TitanTreasuryCharacters[ Faction ], function( Realm, rData )
                                   if ( not tbl[ Realm ] ) then tbl[ Realm ] = {}; end
                                   table.foreach( rData, function( Player, pData )
                                             if ( not tbl[ Realm ][ Player ] ) then tbl[ Realm ][ Player ] = {}; end
                                             tbl[ Realm ][ Player ][ "Faction" ] = Faction;
                                             if ( Faction == "Neutral" ) then
                                                  tbl[ Realm ][ Player ][ "Badge" ] = "FFA";
                                             else
                                                  tbl[ Realm ][ Player ][ "Badge" ] = Faction;
                                             end
                                             table.foreach( pData, function( Key, Value )
                                                       Value = TitanTreasury_CoreUtils( "MakeSafe", Value )
                                                       tbl[ Realm ][ Player ][ Key ] = Value;
                                                  end
                                             )
                                        end
	                           )
                              end
                         )
                    end
               end
          )
          TitanTreasuryCharacters = tbl;
          TitanTreasury_DebugUtils( "Info", "convertDB", 463, LB["TITAN_TREASURY_ARRAY"]:format( "TitanTreasuryCharacters" ) );
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_DebugUtils( d, d1, d2, d3, d4, d5 )
-- DESC : For debug messaging and print messages to chat frame
-- **************************************************************************
function TitanTreasury_DebugUtils( d1, d2, d3, d4, d5 )
     local i, d = 1, { [1] = d1, [2] = d2, [3] = d3, [4] = d4, [5] = d5, };
     --[d1    ][d2      ][d3  ][d4     ]
     --[Action][Function][Line][Message]
     if ( d[1] == "Info" ) then
	  DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_TREASURY_CORE_ITAG"]:format( d[4] ) );
     elseif ( d[1] == "Error" ) then
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_TREASURY_CORE_ETAG"]:format( d[4] ) );
     elseif ( d[1] == "Bug" ) then 
          if ( TitanTreasuryDebug.State ) then
               local fError = ( "[%s] %s" ):format( TitanTreasury_ColorUtils( "GetCText", "orange", d[2] ), d[4] );
               DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_TREASURY_CORE_DTAG"]:format( fError ) );
               if ( TitanTreasuryDebug.LogEvent ) then tinsert( TitanTreasuryDebug.Log, d[4] ); end
          end
     elseif ( d[1] == "Toggle" ) then
          if ( TitanTreasuryDebug[ d[2] ] ) then 
               TitanTreasuryDebug[ d[2] ] = false;
	       TitanTreasury_DebugUtils( "Info", "", "", "Debugging now OFF" );
          elseif ( not TitanTreasuryDebug[ d[2] ] ) then
               TitanTreasuryDebug[ d[2] ] = true;
	       TitanTreasury_DebugUtils( "Info", "", "", "Debugging now ON" );
          end
     elseif ( d[1] == "gTest" ) then
          -- testing trigger
          -- /script TitanTreasury_DebugUtils( "gTest", "rep", 514, "This is a test" );
          if ( not TitanTreasuryDebug.Temp or TitanTreasuryDebug.Temp == nil ) then TitanTreasuryDebug.Temp = {}; end
          if ( not TitanTreasuryDebug.Temp.HTML or TitanTreasuryDebug.HTML == nil ) then TitanTreasuryDebug.Temp.HTML = {}; end
          if ( d[2] == "rep" ) then
               for i = 1, 8 do
                    local test, test2, test3, newHex = "", "", "", "";
                    local standing = TitanTreasuryStaticArrays.Standings.colors.rep[ i ];
                    j = 1;
                    while ( j <= 100 ) do
                         newHex = string.sub( standing[ j ], 5, 10 );
                         test = ( "%s<td bgcolor='#%s' width='1px'>&nbsp;</td>" ):format( test, newHex );
                         test2 = ( "%s|cff%s/|r" ):format( test2, newHex );
                         test3 = ( "%s|cff%sl|r" ):format( test3, newHex );
                         j = j + 1;
                    end
                    if ( TitanTreasuryDebug.State ) then
                         if ( not TitanTreasuryDebug.Temp.HTML[ i ] or TitanTreasuryDebuf.Temp.HTML[ i ] == nil ) then TitanTreasuryDebug.Temp.HTML[ i ] = {}; end
                         TitanTreasuryDebug.Temp.HTML[ i ] = ( "<table align='center' width='100px' cellpadding='0' cellspacing='0'><tr>%s</tr></table>" ):format( test );
                    else
                         TitanTreasuryDebug.Temp.HTML = {};
                    end
                    TitanTreasury_DebugUtils( "Info", "gTest", 514, test2 );
               end
          elseif ( d[2] == "progress" ) then
               local temp, test, test2, test3 = TitanTreasury_ColorUtils( "GetCGradient", "progress", 2, 100, "test" );
               if ( TitanTreasuryDebug.State ) then
                    if ( not TitanTreasuryDebug.Temp.HTML[ i ] or TitanTreasuryDebuf.Temp.HTML[ i ] == nil ) then TitanTreasuryDebug.Temp.HTML[ i ] = {}; end
                    TitanTreasuryDebug.Temp.HTML[ i ] = ( "<table align='center' width='100px' cellpadding='0' cellspacing='0'><tr>%s</tr></table>" ):format( test );
               else
                    TitanTreasuryDebug.Temp.HTML = {};
               end
               if ( d[3] ) then return test3; end
               TitanTreasury_DebugUtils( "Info", "gTest", 525, test2 );
          end
          TitanTreasury_DebugUtils( "Info", "gTest", 527, "Gradient Test Complete!" );
     else
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_TREASURY_DEBUG_CMDE"]:format( d[1], d[2], d[3] ) );
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_ResetUtils( r1, r2, r3, r4, r5 )
-- DESC : Diffrent ways to fix / reset Data Array
-- **************************************************************************
function TitanTreasury_ResetUtils( r1, r2, r3, r4, r5 )
     local r = { [1] = r1, [2] = r2, [3] = r3, [4] = r4, [5] = r5, };
     --[r1    ][r2    ][r3  ][r4   ]
     --[reload][action][pram][value]
     if ( r[2] == "reset" ) then
          TitanTreasury = nil;
          TitanTreasuryInit = nil;
          TitanTreasuryStaticArrays = nil;
          TitanTreasuryLocals = nil;
          TitanTreasuryDebug = nil;
          TitanTreasuryProfile = nil;
          TitanTreasuryCharacters = nil;
          TitanTreasuryTokens = nil;
          TitanTreasury_Init( "init" );
     elseif ( r[2] == "checkdb" ) then
          TitanTreasury_Init( "checkDB" );
     else 
          TitanTreasury_DebugUtils( "Error", "TitanTreasury_ResetUtils", 554, LB["TITAN_TREASURY_HELP_CMD_UNKNOWN"] );
     end
     if ( r[1] ) then ReloadUI(); end
end

-- **************************************************************************
-- NAME : TitanPanelMoney_ColorUtils( c1, c2, c3, c4, c5, c6 )
-- DESC : for coloring the tooltip and messages
-- **************************************************************************
function TitanTreasury_ColorUtils( c1, c2, c3, c4, c5, c6 )
     local c = { [1] = c1, [2] = c2, [3] = c3, [4] = c4, [5] = c5, [6] = c6, };
     if ( c[1] == "GetCText" ) then
          --[c1    ][c2   ][c3  ]
          --[Action][Color][Text]
          local color = strupper( c[2] );
          local colorCode
          local colorCache = {
               GREEN = "|cff00ff00%s|r",
               BLUE = "|cff00ffff%s|r",
               GRAY = "|cff808080%s|r",
               SGRAY = "|cff98afc7%s|r",
               WHITE = "|cffffffff%s|r",
               YELLOW = "|cffffd200%s|r",
               ORANGE = "|cffff9900%s|r",
               DORANGE = "|cffcc6600%s|r",
               LIME = "|cffaaf200%s|r",
               PINK = "|cfff48cba%s|r",
               PINK2 = "|cffec799a%s|r",
               RED = "|cffff0000%s|r",
               GOLD = "|cffffd700%s|r",
               SILVER = "|cffc0c0c0%s|r",
               COPPER = "|cffb87333%s|r",
               VIOLET = "|cff8C489F%s|r",
          }
          colorCode = colorCache[color]:format( c[3] );
          return colorCode;
     elseif ( c[1] == "GetCStanding" ) then
          -- [c1    ][c2      ][c3     ]
          -- [Action][Standing][Percent]
          local colorCode, pText, cText
          if ( c[3] == "label" ) then 
               pText = _G[ ( "FACTION_STANDING_LABEL%s" ):format( c[2] ) ];
          else
               pText = c[3];
          end
          if ( TitanTreasury.Guild.Style == 1 ) then
               colorCode = pText;
          elseif ( TitanTreasury.Guild.Style == 2 ) then
               colorCode = TitanTreasuryStaticArrays.Standings.colors.labels[ c[2] ]:format( pText )
          end
          return colorCode
     elseif ( c[1] == "GetCDemo" ) then
          -- [c1    ][c2      ]
          -- [Action][Standing]
          local i, demo, cText = 1, "", {};
          if ( c[2] ) then
               cText = TitanTreasuryStaticArrays.Standings.colors.rep[ c[2] ];
          else
               cText = TitanTreasuryStaticArrays.Standings.colors.progress;
          end
          while ( i <= 100 ) do
               demo = ( "%s%s" ):format( demo, cText[i]:format( "l" ) );
               i = i + 1;
          end
          collectgarbage( "collect" )
          return demo
     elseif ( c[1] == "GetCFaction" ) then
          -- [c1    ][c2     ]
          -- [Action][Faction]
          local color = string.upper( c[2] )
          local colorCode
          local colorCache = {
               NEUTRAL = "|cffc0c0c0%s|r",
               ALLIANCE = "|cff4954e8%s|r",
               HORDE = "|cffe50c11%s|r",
          }
          colorCode = colorCache[color]:format( c[3] );
          return colorCode;
     elseif ( c[1] == "GetCClass" ) then
          --[c1    ][c2   ][c3    ]
          --[Action][Class][Player]
          local classEng, localClass, colorCode;
          colorCode = "|cff808080%s|r";
          if ( c[2] == LB["TITAN_TREASURY_CONFIG_UNKNOWN"] ) then 
               colorCode = "|cff808080%s|r";
          else
               classEng = TitanTreasuryLocals.ToEnglish[c[2]];
               localClass = TitanTreasuryLocals.LocalClass[classEng];
               colorCode = TitanTreasuryLocals.ColorClass[classEng];
          end
          return colorCode:format( c[3] );
     elseif ( c[1] == "GenCGradient" ) then
          --[c1    ]
          --[Action]
          local test, test2, test3, vs, ve = "", "", "", "", "";
          local standings = {};
          local lThis = { ["rep"] = 8, ["progress"] = 1, }
          local cSteps = 51;
          table.foreach( lThis, function( k, v )
                    local l = 1;
                    while ( l <= v  ) do
                         local fromColor, toColor, stepColor, gradients, ctUse = {}, {}, {}, {}, {};
                         if ( k == "rep" ) then
                              if ( l == 1 ) then
                                   vs = l; ve = l;
                              else
                                   vs = l - 1; ve = l;
                              end
                         else
                                   vs = l; ve = l + 1;
                         end
                         ctUse = {
                              [1] = TitanTreasuryStaticArrays.Standings["base"][ k ][ vs ],
                              [2] = TitanTreasuryStaticArrays.Standings["base"][ k ][ ve ],
                         };
                         fromColor.r, fromColor.g, fromColor.b = TitanTreasury_CoreUtils( "HexToDec", ctUse[1] );
                         toColor.r, toColor.g, toColor.b = TitanTreasury_CoreUtils( "HexToDec", ctUse[2] );
                         stepColor = {
                              r = ( fromColor.r - toColor.r ) / ( cSteps - 1 ),
                              g = ( fromColor.g - toColor.g ) / ( cSteps - 1 ),
                              b = ( fromColor.b - toColor.b ) / ( cSteps - 1 ),
                         };
                         local h, j, i = 1, 1, .1
                         while ( i <= cSteps ) do
                              r = math.floor( fromColor.r - ( stepColor.r * i ) );
                              g = math.floor( fromColor.g - ( stepColor.g * i ) );
                              b = math.floor( fromColor.b - ( stepColor.b * i ) );
                              r = r <= 255 and r >= 0 and r or 0
                              g = g <= 255 and g >= 0 and g or 0
                              b = b <= 255 and b >= 0 and b or 0               
                              newHex = ( "%02x%02x%02x" ):format( r, g, b );
                              if ( j == 5 ) then 
                                   if ( h <= 100 ) then
                                        gradients[ h ] = ( "|cff%s%%s|r" ):format( newHex );
                                        if ( h == 100 and k == "rep" ) then tinsert( standings, ( "|cff%s%%s|r" ):format( newHex ) ); end
                                   end
                                   h = h + 1; j = 1;
                              else
                                   j = j + 1;
                              end
                              i = i + .1;
                         end
                         if ( not TitanTreasuryStaticArrays.Standings.colors[ k ] ) then TitanTreasuryStaticArrays.Standings.colors[ k ] = {}; end
                         if ( k == "rep" ) then
                              TitanTreasuryStaticArrays.Standings.colors[ k ][ l ] = gradients;
                         else
                              TitanTreasuryStaticArrays.Standings.colors[ k ] = gradients;
                         end
                         l = l + 1;
                    end
                    TitanTreasuryStaticArrays.Standings.colors[ "labels" ] = standings;
               end
          );
     elseif ( c[1] == "GetCGradient" ) then
          -- [c1    ][c2   ][c3      ][c4     ][c5  ]
          -- [Action][Style][Standing][Percent][Text]
          local cText, rText
          if ( type( c[4] ) == "number" and c[4] == 0 ) then c[4] = tonumber( 1 ); end
          if ( type( c[5] ) == "number" and c[5] == 0 ) then c[5] = tonumber( c[5] ); end
          if ( c[2] == "rep" ) then
               cText = TitanTreasuryStaticArrays.Standings.colors[ c2 ][ c[3] ][ c[4] ];
          elseif ( c[2] == "progress" ) then
               cText = TitanTreasuryStaticArrays.Standings.colors[ c2 ][ c[4] ];
          end
          if ( c[5] ) then
               rText = cText:format( c[5] );
          else
               rText = cText;
          end
          return rText;
     elseif ( c[1] == "GetRandomHex" ) then
          local newHex
          local r, g, b = random( ), random( ), random( );
          r = r * 255; g = g * 255; b = b * 255;
          newHex = ( "%02x%02x%02x" ):format( r, g, b );
          return newHex;
     end
end
