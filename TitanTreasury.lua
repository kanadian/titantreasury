-- **************************************************************************
-- * TitanTreasury.lua
-- *
-- * By: KanadiaN
-- *       (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanTreasury", true );

-- **************************************************************************
-- NAME : TitanTreasury_Init( action )
-- DESC : For first time addon use
-- **************************************************************************
function TitanTreasury_Init( action )
     if ( action == "checkDB" ) then
          checkDB = true;
          TitanTreasuryInit = nil;
     elseif ( action == "init" ) then
          checkDB = false;
     end
     if ( not TitanTreasury or TitanTreasury == nil ) then TitanTreasury = {}; end
     if ( not TitanTreasuryInit or TitanTreasuryInit == nil ) then
          if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 24, LB["TITAN_TREASURY_MENU_CHECKDB"] ); end
	  if ( not TitanTreasuryStaticArrays or TitanTreasuryStaticArrays == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 26, LB["TITAN_TREASURY_ARRAY"]:format( "TitanTreasuryStaticArrays" ) ); end
               TitanTreasuryStaticArrays = {};
          end
          if ( not TitanTreasuryStaticArrays.Modes or TitanTreasuryStaticArrays.Modes == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 30, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "Modes" ) ); end
               TitanTreasuryStaticArrays.Modes = {
                    { Name = LB["TITAN_TREASURY_CONFIG_SERVER"], Value = 1, },
                    { Name = LB["TITAN_TREASURY_CONFIG_PLAYER"], Value = 2, },
               };
          end
          if ( not TitanTreasuryStaticArrays.Types or TitanTreasuryStaticArrays.Types == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 37, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "Types" ) ); end
               TitanTreasuryStaticArrays.Types = {
                    { Name = LB["TITAN_TREASURY_CONFIG_TEXT"], Value = string.upper( LB["TITAN_TREASURY_CONFIG_TEXT"] ), },
                    { Name = LB["TITAN_TREASURY_CONFIG_COIN"], Value = string.upper( LB["TITAN_TREASURY_CONFIG_COIN"] ), },
               };
          end
          if ( not TitanTreasuryStaticArrays.Styles or TitanTreasuryStaticArrays.Styles == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 44, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "Styles" ) ); end
               TitanTreasuryStaticArrays.Styles = {
                    { Name = LB["TITAN_TREASURY_CONFIG_PLAIN"], Value = 1, },
                    { Name = LB["TITAN_TREASURY_CONFIG_COLORED"], Value = 2, },
               };
          end
          if ( not TitanTreasuryStaticArrays.sTypes or TitanTreasuryStaticArrays.sTypes == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 51, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "sTypes" ) ); end
               TitanTreasuryStaticArrays.sTypes = { LB["TITAN_TREASURY_CONFIG_NAME"], LB["TITAN_TREASURY_CONFIG_GOLD"], LB["TITAN_TREASURY_CONFIG_LEVEL"], LB["TITAN_TREASURY_CONFIG_CLASS"], LB["TITAN_TREASURY_CONFIG_FACTION"], };
          end
          if ( not TitanTreasuryStaticArrays.sMethod or TitanTreasuryStaticArrays.sMethod == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 55, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "sMethod" ) ); end
               TitanTreasuryStaticArrays.sMethod = { LB["TITAN_TREASURY_CONFIG_ASCENDING"], LB["TITAN_TREASURY_CONFIG_DESCENDING"], };
          end
          if ( not TitanTreasuryStaticArrays.iSize or TitanTreasuryStaticArrays.iSize == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 59, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "iSize" ) ); end
               TitanTreasuryStaticArrays.iSize = { { Name = LB["TITAN_TREASURY_CONFIG_SMALL"], Value = 16, }, { Name = LB["TITAN_TREASURY_CONFIG_LARGE"], Value = 32, }, };
          end
          if ( not TitanTreasuryStaticArrays.Standings or TitanTreasuryStaticArrays.Standings == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 63, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "Standings" ) ); end
               TitanTreasuryStaticArrays.Standings = {
                    ["base"] = {
                         ["rep"] = { "8b0000", "ff1919", "ff8C00", "c0c0c0", "ffffff", "19ff19", "4169e1", "9932cc", },
                         ["progress"] = { "e6cc80", "ff8c00" },
                    },
                    ["colors"] = {
                         ["rep"] = {},
                         ["progress"] = {},
                         ["labels"] = {},
                    },
               };
          end
          if ( not TitanTreasuryStaticArrays.cID or TitanTreasuryStaticArrays.cID == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 77, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "cID" ) ); end
               TitanTreasuryStaticArrays.cID = {
                    [LB["TITAN_TREASURY_VALOR"]] = 396,
               };
          end
          if ( not TitanTreasuryStaticArrays.dTypes or TitanTreasuryStaticArrays.dTypes == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 83, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "dTypes" ) ); end
               TitanTreasuryStaticArrays.dTypes = {
                    { Name = LB["TITAN_TREASURY_CONFIG_LINE_TYPE_NARROW"], Value = 1, },
                    { Name = LB["TITAN_TREASURY_CONFIG_LINE_TYPE_WIDE"], Value = 2, },
               };
          end
          if ( not TitanTreasuryStaticArrays.dStyles or TitanTreasuryStaticArrays.dStyles == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 90, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "dStyles" ) ); end
               TitanTreasuryStaticArrays.dStyles = {
                    { Name = LB["TITAN_TREASURY_CONFIG_LINE_STYLE_MINUS"], Value = 1, },
                    { Name = LB["TITAN_TREASURY_CONFIG_LINE_STYLE_EQUAL"], Value = 2, },
                    { Name = LB["TITAN_TREASURY_CONFIG_LINE_STYLE_STARS"], Value = 3, },
                    { Name = LB["TITAN_TREASURY_CONFIG_LINE_STYLE_NUMBER"], Value = 4, },
               };
          end
          if ( not TitanTreasuryStaticArrays.dColors or TitanTreasuryStaticArrays.dColors == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 99, LB["TITAN_TREASURY_STATICARRAY_FIXED"]:format( "dColors" ) ); end
                    TitanTreasuryStaticArrays.dColors = {
                         { Name = "BLUE", Index = 1, Value = ( "BLUE" ), },
                         { Name = "CLASS", Index = 2, Value = ( "CLASS" ), },
                         { Name = "COPPER", Index = 3, Value = ( "COPPER" ), },
                         { Name = "DORANGE", Index = 4, Value = ( "DORANGE" ), },
                         { Name = "GOLD", Index = 5, Value = ( "GOLD" ), },
                         { Name = "GRAY", Index = 6, Value = ( "GRAY" ), },
                         { Name = "GREEN", Index = 7, Value = ( "GREEN" ), },
                         { Name = "LIME", Index = 8, Value = ( "LIME" ), },
                         { Name = "ORANGE", Index = 9, Value = ( "ORANGE" ), },
                         { Name = "PINK", Index = 10, Value = ( "PINK" ), },
                         { Name = "RED", Index = 11, Value = ( "RED" ), },
                         { Name = "SGRAY", Index = 12, Value = ( "SGRAY" ), },
                         { Name = "SILVER", Index = 13, Value = ( "SILVER" ), },
                         { Name = "WHITE", Index = 14, Value = ( "WHITE" ), },
                         { Name = "YELLOW", Index = 15, Value = ( "YELLOW" ), },
                    };
          end
	  if ( not TitanTreasuryCharacters or TitanTreasuryCharacters == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 119, LB["TITAN_TREASURY_ARRAY"]:format( "TitanTreasuryCharacters" ) ); end
               TitanTreasuryCharacters = {};
          end
	  if ( not TitanTreasuryLocals or TitanTreasuryLocals == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 123, LB["TITAN_TREASURY_ARRAY"]:format( "TitanTreasuryLocals" ) ); end
               TitanTreasuryLocals = {};
          end
	  if ( not TitanTreasuryLocals.LocalClass or TitanTreasuryLocals.LocalClass == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 127, LB["TITAN_TREASURY_LOCALSARRAY_FIXED"]:format( "LocalClass" ) ); end
               TitanTreasuryLocals.LocalClass = {};
          end
	  if ( not TitanTreasuryLocals.ColorClass or TitanTreasuryLocals.ColorClass == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 131, LB["TITAN_TREASURY_LOCALSARRAY_FIXED"]:format( "ColorClass" ) ); end
               TitanTreasuryLocals.ColorClass = {};
          end
	  if ( not TitanTreasuryLocals.ToEnglish or TitanTreasuryLocals.ToEnglish == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 135, LB["TITAN_TREASURY_LOCALSARRAY_FIXED"]:format( "ToEnglish" ) ); end
               TitanTreasuryLocals.ToEnglish = {};
          end
          if ( not TitanTreasuryDebug or TitanTreasuryDebug == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 139, LB["TITAN_TREASURY_ARRAY"]:format( "TitanTreasuryDebug" ) ); end
               TitanTreasuryDebug = {
                    State = false,
                    Event = false,
                    LogEvent = false,
                    dLine = "",
                    FormatToRemove = LB["TITAN_TREASURY_CONFIG_NONE"], 
                    Log = {},
                    Temp = {},
               };
          end
          if ( not TitanTreasury.General or TitanTreasury.General == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 151, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "General" ) ); end
               TitanTreasury.General = {
                    ArrayVersion = LB["TITAN_TREASURY_CORE_DBVER"],
                    DisplayB = true,
                    ShowMem = true,
                    ShowAlts = false,
                    ShowGuildRep = false,
                    ShowTokens = false,
                    ShowAll = false,
                    ShowBadge = false,
                    ShowProfs = false,
                    tooltipTimer = true,
                    ShowShift = false,
                    ShowAlt = false,
                    curRealm = false,
                    Summary = false,
                    levelSummary = true,
                    ttHelp = false,
                    days = 1,
                    hours = 1,
                    minutes = 1,
                    seconds = 1,
                    Watched = 1,
                    dType = 1,
                    dStyle = 1,
                    timerInterval = 2,
                    dColor = "WHITE",
                    Version = LB["TITAN_TREASURY_CORE_VERSION"],
               };
          end
          if ( not TitanTreasury.Colors or TitanTreasury.Colors == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 182, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Colors" ) ); end
               TitanTreasury.Colors = {
                    Days = "cc6600",
                    Hours = "cc6600",
                    Minutes = "cc6600",
                    Seconds = "cc6600",
                    Numbers = "ff9900",
                    Realm = "e9ea52",
               };
          end
          if ( not TitanTreasury.Abbreviations or TitanTreasury.Abbreviations == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 193, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Abbreviations" ) ); end
               TitanTreasury.Abbreviations = {
                    Days = { "Days", "Dys", "D", "d", },
                    Hours = { "Hours", "Hrs", "H", "h", },
                    Minutes = { "Minutes", "Mins", "M", "m", },
                    Seconds = { "Seconds", "Secs", "S", "s", },
               };
          end
          if ( not TitanTreasury.Sort or TitanTreasury.Sort == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 202, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Sort" ) ); end
               TitanTreasury.Sort = {
                    sType = LB["TITAN_TREASURY_CONFIG_NAME"],
                    sMethod = LB["TITAN_TREASURY_CONFIG_ASCENDING"],
               };
          end
          if ( not TitanTreasury.Button or TitanTreasury.Button == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 209, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Button" ) ); end
               TitanTreasury.Button = {
                    Mode = 1,
                    Type = string.upper( LB["TITAN_TREASURY_CONFIG_TEXT"] ),
                    Style = 1,
               };
          end
          if ( not TitanTreasury.Guild or TitanTreasury.Guild == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 217, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Guild" ) ); end
               TitanTreasury.Guild = {
                    ShowGain = false,
                    ShowPercent = false,
                    ShowRaw = false,
                    ShowRemain = false,
                    ShowRepText = false,
                    Style = 1,
               };
          end
          if ( not TitanTreasury.Sessions or TitanTreasury.Sessions == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 228, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Sessions" ) ); end
               TitanTreasury.Sessions = {
                    ShowSession = false,
                    ShowHourly = false,
	       };
          end
          if ( not TitanTreasury.Coins or TitanTreasury.Coins == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 235, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Coins" ) ); end
               TitanTreasury.Coins = {
                    Type = string.upper( LB["TITAN_TREASURY_CONFIG_TEXT"] ),
                    Style = 1,
                    sType = LB["TITAN_TREASURY_CONFIG_NAME"],
                    sMethod = LB["TITAN_TREASURY_CONFIG_ASCENDING"],
               };
          end
          if ( not TitanTreasury.Tokens or TitanTreasury.Tokens == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 244, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Tokens" ) ); end
               TitanTreasury.Tokens = {
                    Icon = false,
                    Name = false,
                    Colored = 1,
                    IconSize = 16,
	       };
          end
          if ( not TitanTreasury.Professions or TitanTreasury.Professions == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 253, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "Professions" ) ); end
               TitanTreasury.Professions = {
                    Icon = false,
                    Name = false,
                    Colored = 1,
                    IconSize = 16,
	       };
          end
          if ( not TitanTreasury.AddAuth or TitanTreasury.AddAuth == nil ) then
               if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 262, LB["TITAN_TREASURY_ARRAY_FIXED"]:format( "AddAuth" ) ); end
               TitanTreasury.AddAuth = {};
          end
          TitanTreasuryInit = true;
          if ( checkDB ) then TitanTreasury_DebugUtils( "Info", "Init", 266, LB["TITAN_TREASURY_MENU_CHECKDB_FINSIHED"] ); end
     end
end

-- **************************************************************************
-- NAME : TitanPanelTreasuryButton_OnEvent( self, event, ... )
-- DESC : Event Handler
-- **************************************************************************
function TitanPanelTreasuryButton_OnEvent( self, event, ... )
     local arg1, arg2, arg3 = ...
     if ( TitanTreasuryDebug and TitanTreasuryDebug.State ) then TitanTreasury_DebugUtils( "Bug", "OnEvent", 276, ( "[%s]" ):format( TitanTreasury_ColorUtils( "GetCText", "gray", event ) ) ); end
     if ( event == "ADDON_LOADED" and ... == "TitanTreasury" ) then
          TitanTreasury_Init( "init" );
     elseif ( event == "COMBAT_TEXT_UPDATE" ) then
          if ( arg1 == "FACTION" ) then
               -- TitanTreasury_DebugUtils( "Bug", "OnEvent", 255, ( "COMBAT_TEXT_UPDATE: Faction: %s changed by %s" ):format( arg2, arg3 ) );
               TitanTreasury_CoreUtils( "UpdateSession", arg2, arg3 );
          end
     elseif ( event == "PLAYER_MONEY" ) then
          TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ].rGold = GetMoney();
          TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
          -- TitanPanelButton_UpdateTooltip();
     elseif ( event == "CHAT_MSG_SYSTEM" ) then
          if ( string.find( arg1, LB["TITAN_TREASURY_NEW_TRADESKILL"] ) or string.find( arg1, LB["TITAN_TREASURY_REMOVED_TRADESKILL"] ) ) then -- "You have learned a new ability:%s"
               local trades = { "45357", "51005", "2550", "2366", "2108", "7411", "3908", "2018", "8613", "131474", "2575", "3273", "78670", "2259", "4036", }
               local s1 = string.match( arg1, "|Hspell:(%d+)|h.([^|]+).|h|r" ); -- Spell ID
               table.foreach( trades, function( _, sID ) if ( s1 == sID ) then TitanTreasury_BuildProfessionsPanel(); end end )
          elseif ( string.find( arg1, LB["TITAN_TREASURY_ADD_CURRENCY"] ) ) then -- "You receive currency:%s"
               TitanTreasury_BuildTokensPanel();
          end
     elseif ( event == "PLAYER_LEVEL_UP" ) then
          TitanTreasury_CoreUtils( "userLeveled", arg1 );
     elseif ( event == "PLAYER_ENTERING_WORLD" ) then
          if ( TitanTreasury.General.DisplayB ) then TitanTreasury_CoreUtils( "DisplayB" ); end
          TitanTreasury_CoreUtils( "setUser" );
     elseif ( event == "PLAYER_LEAVING_WORLD" ) then
          -- Many thanks to esiemiat for pointing this "BUG" out
          TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ].Gold = TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ].rGold;
     elseif ( event == "TIME_PLAYED_MSG" ) then
          TitanTreasury_CoreUtils( "setTimePlayed", arg1, arg2 );
     end
end

-- **************************************************************************
-- NAME : TitanPanelTreasuryButton_GetButtonText()
-- DESC : Build Button Text
-- **************************************************************************
function TitanPanelTreasuryButton_GetButtonText()
     local buttonRichText;
     local sTotal = 0;
     if ( TitanTreasury.Button.Mode == 1 ) then
          local sTable = TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ];
          table.foreach( sTable, function( Key, Data )
                    if ( Data.Faction == TitanTreasuryProfile.Faction ) then
                         if ( Key == TitanTreasuryProfile.Player ) then
                              sTotal = sTotal + Data.rGold;
	                 elseif ( not Data.Hide ) then
                              sTotal = sTotal + Data.Gold;
                         end
                    end
               end
	  )
     elseif ( TitanTreasury.Button.Mode == 2 ) then
          sTotal = GetMoney();
     end
     if ( TitanTreasury.General.ShowBadge ) then
          image = TitanTreasury_CoreUtils( "getBadge", TitanTreasuryProfile.Faction );
          buttonRichText = ( "%s %s" ):format( image, TitanTreasury_CoreUtils( "GetMoney", sTotal, "Button" ) );
     else
          buttonRichText = ( "%s" ):format( TitanTreasury_CoreUtils( "GetMoney", sTotal, "Button" ) );
     end
     return LB["TITAN_TREASURY_BUTTON_LABEL"], buttonRichText;
end

-- **************************************************************************
-- NAME : TitanPanelTreasuryButton_OnClick( self, button )
-- DESC : Toggles Friends Dialog
-- **************************************************************************
function TitanPanelTreasuryButton_OnClick( self, button )
     if ( button == "LeftButton" ) then
          ToggleCharacter( "TokenFrame" );
     else
          TitanPanelButton_OnClick( self, button );
     end
end

-- **************************************************************************
-- NAME : TitanPanelTreasuryButton_OnEnter()
-- DESC : Starts repeating timer when plugin is visible
-- **************************************************************************
function TitanPanelTreasuryButton_OnEnter()
     local sTable = TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ];
     if ( sTable.Gold ~= sTable.rGold and TitanTreasury.Sessions.ShowSession ) then
          TitanTreasury_CoreUtils( "tooltipTimer", "start" );
     end
end

-- **************************************************************************
-- NAME : TitanPanelTreasuryButton_OnLeave()
-- DESC : Stops repeating timer when plugin is hidden
-- **************************************************************************
function TitanPanelTreasuryButton_OnLeave()
     TitanTreasury_CoreUtils( "tooltipTimer", "stop" );
end

StaticPopupDialogs["TITAN_TREASURY_DATABASE_RESET"] = {
     title = "testing",
     text = LB["TITAN_TREASURY_POPUP_REASET"],
     button1 = LB["TITAN_TREASURY_POPUP_OK"],
     hasTitle = true,
     hasEditBox = false,
     whileDead = true,
     hideOnEscape = true,
     timeout = false,
     OnShow = function()

     end,
     OnAccept = function()

     end,
     StaticPopup_Show( "TITAN_TREASURY_DATABASE_RESET" );
};
