## Interface: 100002
## Title: Titan Panel [|cffeda55fTreasury|r] |cff00aa005.23.7.90207|r
## Notes: Currency Managment Plugin for Titan Panel
## Authors: KanadiaN
## X-Translation: 
## SavedVariables: TitanTreasury, TitanTreasuryInit, TitanTreasuryStaticArrays, TitanTreasuryLocals, TitanTreasuryDebug, TitanTreasuryProfile, TitanTreasuryCharacters
## OptionalDeps: 
## Dependencies: Titan
## Version: r40
## X-Child-of: Titan
## X-Compatible-With: 100002
## X-Translation: None
## X-Category: Interface Enhancements
## X-Website: http://www.curse.com/addons/wow/titantreasury
## X-Email: aaron@kanadian.net

Locals\Locals.xml

TitanTreasuryConfig.lua
TitanTreasury.lua
TitanTreasuryToolTip.lua
TitanTreasuryRightClick.lua
TitanTreasuryUtils.lua

TitanTreasury.xml
