-- **************************************************************************
--   TitanTreasuryConfig.lua
-- 
--   By: KanadiaN
--         (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanTreasury", true );
local Config = LibStub( "AceConfig-3.0" );
local Dialog = LibStub( "AceConfigDialog-3.0" );

-- **************************************************************************
-- NAME : Core Vars
-- DESC : These are here so they do not get changed like the locales would
-- **************************************************************************

local PlugInName = "TitanTreasury";
LB["TITAN_TREASURY_CORE_ID"] = "Treasury";
LB["TITAN_TREASURY_CORE_VERSION"] = GetAddOnMetadata( PlugInName, "Version" );
LB["TITAN_TREASURY_CORE_DTAG"] = "|cffff0000<TitanTreasury Debug>|r %s";
LB["TITAN_TREASURY_CORE_ITAG"] = "|cffff9900<TitanTreasury>|r %s";
LB["TITAN_TREASURY_CORE_ETAG"] = "|cffff9900<TitanTreasury Error>|r %s";
LB["TITAN_TREASURY_CORE_DBVER"] = tonumber( 4 );
LB["TITAN_TREASURY_CORE_ARTWORK"] = "Interface\\ICONS\\INV_Misc_Coin_17";

-- **************************************************************************
-- NAME : TitanTreasury_ConfigUtils( action )
-- DESC : Pull information from .toc for config dialog
-- **************************************************************************
local function TitanTreasury_ConfigUtils( action )
     if ( action == "GetAuthor" ) then
          return GetAddOnMetadata( PlugInName, "Authors" );
     elseif ( action == "GetTranslation" ) then
          return GetAddOnMetadata( PlugInName, "X-Translation" );
     elseif ( action == "GetCategory" ) then
          return GetAddOnMetadata( PlugInName, "X-Category" );
     elseif ( action == "GetEmail" ) then
          return GetAddOnMetadata( PlugInName, "X-Email" );
     elseif ( action == "GetWebsite" ) then
          return GetAddOnMetadata( PlugInName, "X-Website" );
     elseif ( action == "GetVersion" ) then
          return tostring( GetAddOnMetadata( PlugInName, "Version" ) );
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_InitGUI();
-- DESC : Open Config Dialog
-- **************************************************************************
function TitanTreasury_InitGUI()
     Dialog:Open( PlugInName );
end

-- **************************************************************************
-- NAME : _G["TreasuryOpts"]
-- DESC : Config Dialog Setup ( Now Global )
-- **************************************************************************
_G["TreasuryOpts"] = {
     type = "group",
     name = LB["TITAN_TREASURY_CONFIG"], -- "Titan [|cffeda55fTreasury|r]",
     args = {
          confgendesc = {
               type = "description", order = 1,
               name = LB["TITAN_TREASURY_CONFIG_DESC"], -- "An Addon for Titan Panel that addon displays your Currency information for your current character and a list of your alts currency.",
               cmdHidden = true
          },
          cat1 = {
               type = "group", order = 2,
               name = LB["TITAN_TREASURY_CONFIG_GROUP_ABOUT"], -- "About",
               desc = LB["TITAN_TREASURY_CONFIG_GROUP_ABOUT_DESC"], -- "Information about Titan Activity.",
               args = {
                    confversiondesc = {
                         type = "description", order = 1,
                         name = LB["TITAN_TREASURY_ABOUT_VERSION"]:format( TitanTreasury_ConfigUtils( "GetVersion" ) ),
                         cmdHidden = true
                    },
                    confauthordesc = {
                         type = "description", order = 2,
                         name = LB["TITAN_TREASURY_ABOUT_AUTHOR"]:format( TitanTreasury_ConfigUtils( "GetAuthor" ) ),
                         cmdHidden = true
                    },
                    conflangauthordesc = {
                         type = "description", order = 3,
                         name = LB["TITAN_TREASURY_ABOUT_TRANSLATION"]:format( TitanTreasury_ConfigUtils( "GetTranslation" ) ),
                         cmdHidden = true
                    },
                    confcatdesc = {
                         type = "description", order = 4,
                         name = LB["TITAN_TREASURY_ABOUT_CATEGORY"]:format( TitanTreasury_ConfigUtils( "GetCategory" ) ),
                         cmdHidden = true
                    },
                    confemaildesc = {
                         type = "description", order = 5,
                         name = LB["TITAN_TREASURY_ABOUT_EMAIL"]:format( TitanTreasury_ConfigUtils( "GetEmail" ) ),
                         cmdHidden = true
                    },
                    confwebsitedesc = {
                         type = "description", order = 6,
                         name = LB["TITAN_TREASURY_ABOUT_WEBSITE"]:format( TitanTreasury_ConfigUtils( "GetWebsite" ) ),
                         cmdHidden = true
                    },
                    confmemorydesc = {
                         type = "description", order = 7,
                         name = function()
                              UpdateAddOnMemoryUsage();
                              mem = floor( GetAddOnMemoryUsage( PlugInName ) );
                              return LB["TITAN_TREASURY_ABOUT_MEMORY"]:format( mem );
                         end,
                         cmdHidden = true,
                    },
                    LoadBannor = {
                         type = "toggle", order = 8,
                         name = LB["TITAN_TREASURY_CONFIG_BANNOR"], -- "Display Load Bannor",
                         desc = LB["TITAN_TREASURY_CONFIG_BANNOR_DESC"], -- "This will display addon information and memory usage when the addon is loaded in the chat frame.",
                         get = function() return TitanTreasury.General.DisplayB; end,
                         set = function( _, value ) TitanTreasury.General.DisplayB = value; end,
                    },
               },
          },
          tooltip = { -- Settings & Tooltip
               type = "group", order = 3, childGroups = "tab",
               name = LB["TITAN_TREASURY_CONFIG_GROUP_TOOLTIP"], -- "Tooltip",
               desc = LB["TITAN_TREASURY_CONFIG_GROUP_TOOLTIP_DESC"], -- "Settings to change how the tooltip looks.",
               args = {
                    settings = {
                         type = "group", order = 1,
                         name = LB["TITAN_TREASURY_MENU_SETTINGS"], -- Settings
                         desc = function() return ( "%s %s" ):format( LB["TITAN_TREASURY_CONFIG_GROUP_TOOLTIP"], LB["TITAN_TREASURY_MENU_SETTINGS"] ); end,
                         args = {
                              tooltipTimer = { -- Tooltip Update Timer
                                   type = "group", order = 1, inline = true,
                                   name = LB["TITAN_TREASURY_CONFIG_GROUP_TOOLTIP_INLINE_TIMER"], -- "Tooltip Update Timer",
                                   args = {
                                        tTimer = {
                                             type = "toggle", order = 1,
                                             name = LB["TITAN_TREASURY_CONFIG_TOOLTIP_INLINE_TIMER"], -- "Update Timer",
                                             desc = LB["TITAN_TREASURY_CONFIG_TOOLTIP_INLINE_TIMER_DESC"], -- "This will allow the tooltip to update when it is being displayed",
                                             get = function() return TitanTreasury.General.tooltipTimer; end,
                                             set = function( _, value )
                                                  if ( value ~= TitanTreasury.General.tooltipTimer ) then
                                                       TitanTreasury.General.tooltipTimer = value;
                                                       if ( value ) then
                                                            TitanTreasury_CoreUtils( "tooltipTimer", "start" );
                                                       elseif ( not value ) then
                                                            TitanTreasury_CoreUtils( "tooltipTimer", "stop" );
                                                       end
                                                  end
                                             end,
                                        },
                                        timerRange = {
                                             type = "range", order = 2, width = "full",
                                             name = LB["TITAN_TREASURY_CONFIG_TOOLTIP_INLINE_INTERVAL"], --"Timer Interval",
                                             desc = LB["TITAN_TREASURY_CONFIG_TOOLTIP_INLINE_INTERVAL_DESC"], -- "Sets how often the tooltip will update. ( in seconds )",
                                             hidden = function()
                                                  return not TitanTreasury.General.tooltipTimer;
                                             end,
                                             min = 1,
                                             max = 5,
                                             step = 1,
                                             get = function() return TitanTreasury.General.timerInterval; end,
                                             set = function( _, value )
                                                  if ( value ~= TitanTreasury.General.timerInterval ) then
                                                       TitanTreasury.General.timerInterval = value;
                                                       TitanTreasury_CoreUtils( "tooltipTimer", "restart" );
                                                  end
                                             end,
                                        },
                                   },
                              },
                              tooltipSubMenu = {
                                   type = "group", order = 3, inline = true,
                                   name = "Sub Tooltips",
                                   args = {
                                        extGold = {
                                             type = "toggle", order = 1,
                                             name = LB["TITAN_TREASURY_TOOLTIP_EXTGOLD"], -- "Extended Gold",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_EXTGOLD_DESC"], -- "This will show gold for realm and account.",
                                             get = function() return TitanTreasury.General.ShowShift; end,
                                             set = function( _, value ) TitanTreasury.General.ShowShift = value; end,
                                        },
                                        extPlayed = {
                                             type = "toggle", order = 2,
                                             name = LB["TITAN_TREASURY_TOOLTIP_TPLAYED"], -- "Time Played",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_TPLAYED_DESC"], -- "This will show time played for realm and account.",
                                             get = function() return TitanTreasury.General.ShowAlt; end,
                                             set = function( _, value ) TitanTreasury.General.ShowAlt = value; end,
                                        },
                                   },
                              },
                              tooltipDividerStyle = {
                                   type = "group", order = 4, inline = true,
                                   name = LB["TITAN_TREASURY_CONFIG_GROUP_CATEGORY_DIVIDER"], -- "Category Divider Style",
                                   args = {
                                        dStyle = {
                                             type = "select", order = 1,
                                             name = LB["TITAN_TREASURY_CONFIG_DIVIDER_STYLE"], -- "Tooltip Divider Style",
                                             desc = LB["TITAN_TREASURY_CONFIG_DIVIDER_STYLE_DESC"], -- "Changes the category divider style.",
                                             get = function() return TitanTreasury.General.dStyle; end,
                                             set = function( _, value )
                                                  TitanTreasury.General.dStyle = value;
                                                  TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                             end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.dStyles, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.General.dStyle ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end,
                                        },
                                        dType = {
                                             type = "select", order = 2,
                                             name = LB["TITAN_TREASURY_CONFIG_DIVIDER_TYPE"], -- "Tooltip Divider Type",
                                             desc = LB["TITAN_TREASURY_CONFIG_DIVIDER_TYPE_DESC"], -- "Changes the category divider type.",
                                             get = function() return TitanTreasury.General.dType; end,
                                             set = function( _, value )
                                                  TitanTreasury.General.dType = value;
                                                  TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                             end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.dTypes, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.General.dType ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end,
                                        },
                                        dColor = {
                                             type = "select", order = 3,
                                             name = LB["TITAN_TREASURY_CONFIG_DIVIDER_COLOR"], -- "Tooltip Divider Color",
                                             desc = LB["TITAN_TREASURY_CONFIG_DIVIDER_COLOR_DESC"], -- "Changes the category divider color.",
                                             get = function() return TitanTreasury.General.dColor; end,
                                             set = function( _, value )
                                                  TitanTreasury.General.dColor = value;
                                                  TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                             end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.dColors, function( Key, Data )
                                                            if ( Data.Value ~= "CLASS" ) then
                                                                 preList[Data.Value] = TitanTreasury_ColorUtils( "GetCText", Data.Name, Data.Name );
                                                            elseif ( Data.Value == "CLASS" ) then
                                                                 preList[Data.Value] = TitanTreasury_ColorUtils( "GetCClass", UnitClass( "player" ), Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end,
                                        },
                                   },
                              },
                         },
                    },
                    guildreptab = {
                         type = "group", order = 2,
                         name = LB["TITAN_TREASURY_CONFIG_GROUP_GUILDREP"], -- "Guild Rep",
                         desc = LB["TITAN_TREASURY_CONFIG_GROUP_GUILDREP_DESC"], -- "Shows Guild Related Repuataions.",
                         hidden = function() return not TitanTreasury.General.ShowGuildRep; end,
                         args = {
                              ShowGain = {
                                   type = "toggle", order = 1,
                                   name = LB["TITAN_TREASURY_CONFIG_GUILDREP_GAIN"], -- "Show Gains",
                                   desc = LB["TITAN_TREASURY_CONFIG_GUILDREP_GAIN_DESC"], -- "Shows session gain |cffff0000( This might get removed )|r.",
                                   get = function() return TitanTreasury.Guild.ShowGain; end,
                                   set = function( _, value ) TitanTreasury.Guild.ShowGain = value; end,
                              },
                              ShowPercent = {
                                   type = "toggle", order = 2,
                                   name = LB["TITAN_TREASURY_CONFIG_GUILDREP_PERCENT"], -- "Show Percent",
                                   desc = LB["TITAN_TREASURY_CONFIG_GUILDREP_PERCENT_DESC"], -- "Shows persectage of current reputation level.",
                                   get = function() return TitanTreasury.Guild.ShowPercent; end,
                                   set = function( _, value ) TitanTreasury.Guild.ShowPercent = value; end,
                              },
                              ShowRaw = {
                                   type = "toggle", order = 3,
                                   name = LB["TITAN_TREASURY_CONFIG_GUILDREP_RAW"], -- "Show Raw",
                                   desc = LB["TITAN_TREASURY_CONFIG_GUILDREP_RAW_DESC"], -- "Shows raw value data.",
                                   get = function() return TitanTreasury.Guild.ShowRaw; end,
                                   set = function( _, value ) TitanTreasury.Guild.ShowRaw = value; end,
                              },
                              ShowRemain = {
                                   type = "toggle", order = 4,
                                   name = LB["TITAN_TREASURY_CONFIG_GUILDREP_REMAIN"], -- "Show Remaining Rep",
                                   desc = LB["TITAN_TREASURY_CONFIG_GUILDREP_REMAIN_DESC"], -- "Shows the amount of reputation to go.",
                                   get = function() return TitanTreasury.Guild.ShowRemain; end,
                                   set = function( _, value ) TitanTreasury.Guild.ShowRemain = value; end,
                              },
                              ShowRepText = {
                                   type = "toggle", order = 5, width = "full",
                                   name = LB["TITAN_TREASURY_CONFIG_GUILDREP_TEXT"], -- "Show Rep Text",
                                   desc = LB["TITAN_TREASURY_CONFIG_GUILDREP_TEXT_DESC"], -- "Shows reputation level.",
                                   get = function() return TitanTreasury.Guild.ShowRepText; end,
                                   set = function( _, value ) TitanTreasury.Guild.ShowRepText = value; end,
                              },
                              TextStyle = {
                                   type = "select", order = 6,
                                   name = LB["TITAN_TREASURY_CONFIG_GUILDREP_TEXTSTYLE"], -- "Text Style",
                                   desc = LB["TITAN_TREASURY_CONFIG_GUILDREP_TEXTSTYLE_DESC"], -- "Here you can chose if you want colored or plain text.",
                                   get = function() return TitanTreasury.Guild.Style; end,
                                   set = function( _, value ) TitanTreasury.Guild.Style = value; end,
                                   values = function()
                                        local preList = {};
                                        table.foreach( TitanTreasuryStaticArrays.Styles, function( Key, Data )
                                                  if ( Data.Value ~= TitanTreasury.Guild.Style ) then
                                                       preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                  else
                                                       preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                  end
                                             end
                                        );
                                        return preList;
                                   end
                              },
                         },
                    },
                    currencytab = {
                         type = "group", order = 3,
                         name = LB["TITAN_TREASURY_CONFIG_GROUP_COIN"], -- "Currency",
                         desc = LB["TITAN_TREASURY_CONFIG_GROUP_COIN_DESC"], -- "Settings for how currency is displayed.",
                         args = {
                              settingstab = {
                                   type = "group", order = 1,
                                   name = LB["TITAN_TREASURY_MENU_SETTINGS"], -- "Settings",
                                   desc = function() return ( "%s %s" ):format( LB["TITAN_TREASURY_CONFIG_GROUP_COIN"], LB["TITAN_TREASURY_MENU_SETTINGS"] ); end,
                                   args = {
                                        Type = {
                                             type = "select", order = 1,
                                             name = LB["TITAN_TREASURY_CONFIG_COIN_TYPE"], -- "Money Type",
                                             desc = LB["TITAN_TREASURY_CONFIG_COIN_TYPE_DESC"], -- "This will make the money show in text or coin.",
                                             get = function() return TitanTreasury.Coins.Type; end,
                                             set = function( _, value ) TitanTreasury.Coins.Type = value; end,
                                              values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.Types, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.Coins.Type ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        Style = {
                                             type = "select", order = 2,
                                             name = LB["TITAN_TREASURY_CONFIG_COIN_STYLE"], -- "Money Style",
                                             desc = LB["TITAN_TREASURY_CONFIG_COIN_STYLE_DESC"], -- "This will show the money in plain or colored text.",
                                             get = function() return TitanTreasury.Coins.Style; end,
                                             set = function( _, value ) TitanTreasury.Coins.Style = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.Styles, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.Coins.Style ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        sTypes = {
                                             type = "select", order = 3,
                                             name = LB["TITAN_TREASURY_CONFIG_COIN_SORT_TYPES"], -- "Sorting Types :",
                                             desc = LB["TITAN_TREASURY_CONFIG_COIN_SORT_TYPES_DESC"], -- "Different ways to sort the tooltip",
                                             get = function() return TitanTreasury.Sort.sType; end,
                                             set = function( _, value ) TitanTreasury.Sort.sType = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.sTypes, function( key, value )
                                                            if ( TitanTreasury.Sort.sType == value ) then
                                                                 preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                                            else
                                                                 preList[value] = value;
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        sMethod = {
                                             type = "select", order = 4,
                                             name = LB["TITAN_TREASURY_CONFIG_COIN_SORT_METHOD"], -- "Sorting Method :",
                                             desc = LB["TITAN_TREASURY_CONFIG_COIN_SORT_METHOD_DESC"], -- "The method in which the tooltip will be sorted.",
                                             get = function() return TitanTreasury.Sort.sMethod; end,
                                             set = function( _, value ) TitanTreasury.Sort.sMethod = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.sMethod, function( key, value )
                                                            if ( TitanTreasury.Sort.sMethod == value ) then
                                                                 preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                                            else
                                                                 preList[value] = value;
                                                            end
                                                       end
                                                  )
                                                  return preList;
                                             end
                                        },
                                        tooltipCategories = {
                                             type = "group", order = 5, inline = true,
                                             name = LB["TITAN_TREASURY_CONFIG_GROUP_CATEGORY_DISPLAY"], -- "Category Display",
                                             args = {
                                                  ShowGuildRep = {
                                                       type = "toggle", order = 1,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_GUILDREP"], -- "Show Guild Rep",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_GUILDREP_DESC"], -- "This will display your guild reputation in the tooltip.",
                                                       disabled = function() return not IsInGuild(); end,
                                                       get = function() return TitanTreasury.General.ShowGuildRep; end,
                                                       set = function( _, value ) TitanTreasury.General.ShowGuildRep = value; end,
                                                  },
                                                  ShowAlts = {
                                                       type = "toggle", order = 2,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_ALTS"], -- "Show Alts",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_ALTS_DESC"], -- "This will put alts info on the tooltip.",
                                                       get = function() return TitanTreasury.General.ShowAlts; end,
                                                       set = function( _, value ) TitanTreasury.General.ShowAlts = value; end,
                                                  },
                                                  ShowProfs = {
                                                       type = "toggle", order = 3,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_PROFS"], -- "Show Professions",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_PROFS_DESC"], -- "This will put your Profession info on the tooltip.",
                                                       get = function() return TitanTreasury.General.ShowProfs; end,
                                                       set = function( _, value ) TitanTreasury.General.ShowProfs = value; end,
                                                  },
                                                  ShowTokens = {
                                                       type = "toggle", order = 4,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_TOKENS"], -- "Show Tokens",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_TOKENS_DESC"], -- "This will show your tokens on the tooltip.",
                                                       get = function() return TitanTreasury.General.ShowTokens; end,
                                                       set = function( _, value ) TitanTreasury.General.ShowTokens = value; end,
                                                  },
                                                  ShowAll = {
                                                       type = "toggle", order = 5,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_ALL"], -- "Show All Factions",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_ALL_DESC"], -- "This will show all factions in the tooltip.",
                                                       get = function() return TitanTreasury.General.ShowAll; end,
                                                       set = function( _, value )
                                                            if ( value and not TitanTreasury.General.ShowBadge ) then
                                                                 TitanTreasury.General.ShowBadge = value;
                                                                 TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                                            end
                                                            TitanTreasury.General.ShowAll = value;
                                                       end,
                                                  },
                                                  ShowMem = {
                                                       type = "toggle", order = 6,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_SHOWMEM"], -- "Show Memory Usage"
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_SHOWMEM_DESC"], -- "This will show you the amount of memory that TitamEstate is using on the Tooltip."
                                                       get = function() return TitanTreasury.General.ShowMem; end,
                                                       set = function( _, value ) TitanTreasury.General.ShowMem = value; end,
                                                  },
                                             },
                                        },
                                        tooltipExtras = {
                                             type = "group", order = 6, inline = true,
                                             name = LB["TITAN_TREASURY_CONFIG_GROUP_BADGE_DISPLAY"]; -- "Badge Display Options"
                                             args = {
                                                  ShowBadge = {
                                                       type = "toggle", order = 1,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_BADGE"], -- "Show Faction Badge",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_BADGE_DESC"], -- "This will show the character's faction badge.",
                                                       get = function() return TitanTreasury.General.ShowBadge; end,
                                                       set = function( _, value ) 
                                                            if ( not value ) then TitanTreasury.General.ShowAll = value; end
                                                            TitanTreasury.General.ShowBadge = value;
                                                            TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                                       end,
                                                  },
                                                  ShowBadgeText = {
                                                       type = "toggle", order = 2,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_BADGETEXT"], -- "Show Rep Text",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_BADGETEXT_DESC"], -- "This will show the character's faction text in the badge.",
                                                       get = function() return TitanTreasury.General.ShowBadgeText; end,
                                                       set = function( _, value )
                                                            TitanTreasury.General.ShowBadgeText = value;
                                                            TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                                       end,
                                                  },
                                                  ShowBadgeColor = {
                                                       type = "toggle", order = 3,
                                                       name = LB["TITAN_TREASURY_CONFIG_DISPLAY_BADGECOLOR"], -- "Show Rep Text",
                                                       desc = LB["TITAN_TREASURY_CONFIG_DISPLAY_BADGECOLOR_DESC"], -- "Shows reputation level.",
                                                       hidden = function() return not TitanTreasury.General.ShowBadgeText; end,
                                                       get = function() return TitanTreasury.General.ShowBadgeColor; end,
                                                       set = function( _, value )
                                                            TitanTreasury.General.ShowBadgeColor = value;
                                                            TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                                                       end,
                                                  },
                                             },
                                        },
                                   },
                              },
                              tokentab = {
                                   type = "group", order = 2,
                                   name = LB["TITAN_TREASURY_CONFIG_GROUP_TOKEN"], -- "Tokens",
                                   desc = LB["TITAN_TREASURY_CONFIG_GROUP_TOKEN_DESC"], -- "Settings for how tokens are displayed",
                                   hidden = function() return not TitanTreasury.General.ShowTokens; end,
                                   args = {
                                        ShowMinMax = {
                                             type = "toggle", order = 1,
                                             name = LB["TITAN_TREASURY_CONFIG_MINMAX"], -- "Show Min / Max",
                                             desc = LB["TITAN_TREASURY_CONFIG_MINMAX_DESC"], -- "This will display the values in a min / max fashion",
                                             get = function() return TitanTreasury.Tokens.ShowMax end,
                                             set = function( _, value ) TitanTreasury.Tokens.ShowMax = value; end,
                                        },
                                        ShowIcon = {
                                             type = "toggle", order = 2,
                                             name = LB["TITAN_TREASURY_CONFIG_ICON"], -- "Show Icon",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_ICON_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_TOKEN"] ) end, -- "This will show %s icon on the tooltip.",
                                             get = function() return TitanTreasury.Tokens.Icon; end,
                                             set = function( _, value ) TitanTreasury.Tokens.Icon = value; end,
                                        },
                                        ShowName = {
                                             type = "toggle", order = 3, width = "double",
                                             name = LB["TITAN_TREASURY_CONFIG_LABEL"], -- "Show Names",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_LABEL_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_TOKEN"] ); end, -- "This will show the Tokens name on the tooltip.",
                                             hidden = function() return not TitanTreasury.Tokens.Icon; end,
                                             get = function() return TitanTreasury.Tokens.Name; end,
                                             set = function( _, value ) TitanTreasury.Tokens.Name = value; end,
                                        },
                                        IconSize = {
                                             type = "select", order = 4,
                                             name = LB["TITAN_TREASURY_CONFIG_ICON_SIZE"], -- "Icon Size",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_ICON_SIZE_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_TOKEN"] ); end, -- "This will change the size of the icon on the tooltip.",
                                             hidden = function() return not TitanTreasury.Tokens.Icon; end,
                                             get = function() return TitanTreasury.Tokens.IconSize; end,
                                             set = function( _, value ) TitanTreasury.Tokens.IconSize = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.iSize, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.Tokens.IconSize ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        TextStyle = {
                                             type = "select", order = 5,
                                             name = LB["TITAN_TREASURY_CONFIG_STYLE"], -- "Text Style",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_STYLE_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_TOKEN"] ); end, -- "This will make the %s text show up in plain or colored text.",
                                             get = function() return TitanTreasury.Tokens.Colored; end,
                                             set = function( _, value ) TitanTreasury.Tokens.Colored = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.Styles, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.Tokens.Colored ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
		                        AllowedTokens = {
                                             type = "group", order = 6, inline = true,
                                             name = "",
                                             args = {},
                                        },
                                   },
                              },
                              professiontab = {
                                   type = "group", order = 3,
                                   name = LB["TITAN_TREASURY_CONFIG_GROUP_PROFESSION"], -- Professions
                                   desc = LB["TITAN_TREASURY_CONFIG_GROUP_PROFESSION_DESC"], -- This will show your characters profession inthe tooltip
                                   hidden = function() return not TitanTreasury.General.ShowProfs; end,
                                   args = {
                                        ShowMinMax = {
                                             type = "toggle", order = 1,
                                             name = LB["TITAN_TREASURY_CONFIG_MINMAX"], -- "Show Min / Max",
                                             desc = LB["TITAN_TREASURY_CONFIG_MINMAX_DESC"], -- "This will display the values in a min / max fashion",
                                             get = function() return TitanTreasury.Professions.ShowMax end,
                                             set = function( _, value ) TitanTreasury.Professions.ShowMax = value; end,
                                        },
                                        ShowIcon = {
                                             type = "toggle", order = 2,
                                             name = LB["TITAN_TREASURY_CONFIG_ICON"], -- "Show Icon",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_ICON_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_PROFESSION"] ); end, -- "This will show %s icon on the tooltip.",
                                             get = function() return TitanTreasury.Professions.Icon; end,
                                             set = function( _, value ) TitanTreasury.Professions.Icon = value; end,
                                        },
                                        ShowName = {
                                             type = "toggle", order = 3, width = "double",
                                             name = LB["TITAN_TREASURY_CONFIG_LABEL"], -- "Show Names",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_LABEL_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_PROFESSION"] ); end, -- "This will show the %s name on the tooltip.",
                                             hidden = function() return not TitanTreasury.Professions.Icon; end,
                                             get = function() return TitanTreasury.Professions.Name; end,
                                             set = function( _, value ) TitanTreasury.Professions.Name = value; end,
                                        },
                                        IconSize = {
                                             type = "select", order = 4,
                                             name = LB["TITAN_TREASURY_CONFIG_ICON_SIZE"], -- "Icon Size",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_ICON_SIZE_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_PROFESSION"] ); end, -- "This will change the size of the %s icon on the tooltip.",
                                             hidden = function() return not TitanTreasury.Professions.Icon; end,
                                             get = function() return TitanTreasury.Professions.IconSize; end,
                                             set = function( _, value ) TitanTreasury.Professions.IconSize = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.iSize, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.Professions.IconSize ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        TextStyle = {
                                             type = "select", order = 5,
                                             name = LB["TITAN_TREASURY_CONFIG_STYLE"], -- "Text Style",
                                             desc = function() return LB["TITAN_TREASURY_CONFIG_STYLE_DESC"]:format( LB["TITAN_TREASURY_CONFIG_GROUP_PROFESSION"] ); end, -- "This will make the %s text show up in plain or colored text.",
                                             get = function() return TitanTreasury.Professions.Colored; end,
                                             set = function( _, value ) TitanTreasury.Professions.Colored = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.Styles, function( Key, Data )
                                                            if ( Data.Value ~= TitanTreasury.Professions.Colored ) then
                                                                 preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                                            else
                                                                 preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        AllowedProfessions = {
                                             type = "group", order = 6, inline = true,
                                             name = "",
                                             args = {},
                                        },
                                   },
                              },
                         },
                    },
                    playedtab = {
                         type = "group", order = 4,
                         name = LB["TITAN_TREASURY_TOOLTIP_PLAYED"], -- "Played",
                         desc = LB["TITAN_TREASURY_TOOLTIP_PLAYED_DESC"], -- "Setting for how time played is shown.",
                         hidden = function() return not TitanTreasury.General.ShowAlt; end,
                         args = {
                              settings = {
                                   type = "group", order = 1,
                                   name = "Settings",
                                   args = {
                                        level = {
                                             type = "toggle", order = 1,
                                             name = LB["TITAN_TREASURY_CONFIG_LEVEL"], -- "Level",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_LEVEL_DESC"], -- "This will show your characters level summary.",
                                             get = function() return TitanTreasury.General.levelSummary; end,
                                             set = function( _, value ) TitanTreasury.General.levelSummary = value; end,
                                        },
                                        account = {
                                             type = "toggle", order = 2,
                                             name = LB["TITAN_TREASURY_TOOLTIP_ACCOUNT"], -- "Account",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_ACCOUNT_DESC"], -- "This will show account summary.",
                                             get = function() return TitanTreasury.General.Summary; end,
                                             set = function( _, value ) TitanTreasury.General.Summary = value; end,
                                        },
                                        watched = {
                                             type = "toggle", order = 3,
                                             name = LB["TITAN_TREASURY_TOOLTIP_WATCHED"], -- "Realm",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_WATCHED_DESC"], -- "This will show the watched realm.",
                                             get = function() return TitanTreasury.General.curRealm; end,
                                             set = function( _, value ) TitanTreasury.General.curRealm = value; end,
                                        },
                                        watcheddd = {
                                             type = "select", order = 4,
                                             name = LB["TITAN_TREASURY_TOOLTIP_WATCHED_REALMS"], -- "Realms",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_WATCHED_REALMS_DESC"], -- "This will let you select witch realm you would like to watch.",
                                             hidden = function() return not TitanTreasury.General.curRealm; end,
                                             get = function() return TitanTreasury.General.Watched; end,
                                             set = function( _, value ) TitanTreasury.General.Watched = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasuryStaticArrays.realms, function( Key, Name )
                                                            if ( Key ~= TitanTreasury.General.Watched ) then
                                                                 preList[Key] = ( "|cff19ff19%s|r" ):format( Name );
                                                            else
                                                                 preList[Key] = ( "|cffffff9a%s|r" ):format( Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },

                                   },
                              },
                              abbreviation = {
                                   type = "group", order = 2,
                                   name = LB["TITAN_TREASURY_TOOLTIP_ABBREV"], -- "Abbreviations",
                                   desc = LB["TITAN_TREASURY_TOOLTIP_ABBREV_DESC"], -- "This lets you use abbreviations",
                                   args = {
                                        days = {
                                             type = "select", order = 1,
                                             name = LB["TITAN_TREASURY_TOOLTIP_ABBREV_DAYS"], -- "Days",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_ABBREV_DAYS_DESC"], --"Lets you use short form for Days",
                                             get = function() return TitanTreasury.General.days; end,
                                             set = function( _, value ) TitanTreasury.General.days = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasury.Abbreviations.Days, function( Key, Name )
                                                            if ( Key ~= TitanTreasury.General.days ) then
                                                                 preList[Key] = ( "|cff19ff19%s|r" ):format( Name );
                                                            else
                                                                 preList[Key] = ( "|cffffff9a%s|r" ):format( Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        hours = {
                                             type = "select", order = 3,
                                             name = LB["TITAN_TREASURY_TOOLTIP_ABBREV_HOURS"], -- "Hours",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_ABBREV_HOURS_DESC"], -- "Lets you use short form for hours",
                                             get = function() return TitanTreasury.General.hours; end,
                                             set = function( _, value ) TitanTreasury.General.hours = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasury.Abbreviations.Hours, function( Key, Name )
                                                            if ( Key ~= TitanTreasury.General.hours ) then
                                                                 preList[Key] = ( "|cff19ff19%s|r" ):format( Name );
                                                            else
                                                                 preList[Key] = ( "|cffffff9a%s|r" ):format( Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        minutes = {
                                             type = "select", order = 5,
                                             name = LB["TITAN_TREASURY_TOOLTIP_ABBREV_MINUTES"], -- "Minutes",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_ABBREV_MINUTES_DESC"], -- "Lets you use short form for Minutes",
                                             get = function() return TitanTreasury.General.minutes; end,
                                             set = function( _, value ) TitanTreasury.General.minutes = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasury.Abbreviations.Minutes, function( Key, Name )
                                                            if ( Key ~= TitanTreasury.General.minutes ) then
                                                                 preList[Key] = ( "|cff19ff19%s|r" ):format( Name );
                                                            else
                                                                 preList[Key] = ( "|cffffff9a%s|r" ):format( Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                        seconds = {
                                             type = "select", order = 7,
                                             name = LB["TITAN_TREASURY_TOOLTIP_ABBREV_SECONDS"], -- "Seconds",
                                             desc = LB["TITAN_TREASURY_TOOLTIP_ABBREV_SECONDS_DESC"], -- "Lets you use short form for Seconds",
                                             get = function() return TitanTreasury.General.seconds; end,
                                             set = function( _, value ) TitanTreasury.General.seconds = value; end,
                                             values = function()
                                                  local preList = {};
                                                  table.foreach( TitanTreasury.Abbreviations.Seconds, function( Key, Name )
                                                            if ( Key ~= TitanTreasury.General.seconds ) then
                                                                 preList[Key] = ( "|cff19ff19%s|r" ):format( Name );
                                                            else
                                                                 preList[Key] = ( "|cffffff9a%s|r" ):format( Name );
                                                            end
                                                       end
                                                  );
                                                  return preList;
                                             end
                                        },
                                   },
                              },
                              colors = {
                                   type = "group", order = 3,
                                   name = LB["TITAN_TREASURY_TOOLTIP_COLORS"], -- "Color",
                                   desc = LB["TITAN_TREASURY_TOOLTIP_COLORS_DESC"], -- "Colors settings",
                                   args = {
                                        numberscolor = {
                                             type = "color", order = 1,
                                             name = LB["TITAN_TREASURY_TOOLTIP_COLORS_NUMBERS"], -- "Numbers Text Color",
                                             desc = function() return LB["TITAN_TREASURY_TOOLTIP_COLORS_NUMBERS_DESC"]:format( TitanTreasury.Colors.Numbers ); end, -- "Lets you select the color ( %s ) for any Numbers.",
                                             get = function()
                                                  local color = TitanTreasury.Colors.Numbers;
                                                  r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                                  return r, g, b
                                             end,
                                             set = function( _, r, g, b, a )
                                                  r = r * 255; g = g * 255; b = b * 255;
                                                  newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                                  TitanTreasury.Colors.Numbers = newHex;
                                             end,
                                             hasAlpha = false,
                                        },
                                        dayscolor = {
                                             type = "color", order = 2,
                                             name = LB["TITAN_TREASURY_TOOLTIP_COLORS_DAYS"], -- "Days Text Color",
                                             desc = function() return LB["TITAN_TREASURY_TOOLTIP_COLORS_DAYS_DESC"]:format( TitanTreasury.Colors.Days ); end, -- "Lets you select the color ( %s ) for the word Days.",
                                             get = function()
                                                  local color = TitanTreasury.Colors.Days;
                                                  r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                                  return r, g, b
                                             end,
                                             set = function( _, r, g, b, a )
                                                  r = r * 255; g = g * 255; b = b * 255;
                                                  newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                                  TitanTreasury.Colors.Days = newHex;
                                             end,
                                             hasAlpha = false,
                                        },
                                        hourcolor = {
                                             type = "color", order = 3,
                                             name = LB["TITAN_TREASURY_TOOLTIP_COLORS_HOURS"], -- "Hours Text Color",
                                             desc = function() return LB["TITAN_TREASURY_TOOLTIP_COLORS_HOURS_DESC"]:format( TitanTreasury.Colors.Hours ); end, -- "Lets you select the color ( %s ) for the word Hours.",
                                             get = function()
                                                  local color = TitanTreasury.Colors.Hours;
                                                  r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                                  return r, g, b
                                             end,
                                             set = function( _, r, g, b, a )
                                                  r = r * 255; g = g * 255; b = b * 255;
                                                  newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                                  TitanTreasury.Colors.Hours = newHex;
                                             end,
                                             hasAlpha = false,
                                        },
                                        minutecolor = {
                                             type = "color", order = 4,
                                             name = LB["TITAN_TREASURY_TOOLTIP_COLORS_MINUTES"], -- "Minutes Text Color",
                                             desc = function() return LB["TITAN_TREASURY_TOOLTIP_COLORS_MINUTES_DESC"]:format( TitanTreasury.Colors.Minutes ); end, -- "Lets you select the color ( %s ) for the word Minutes.",
                                             get = function()
                                                  local color = TitanTreasury.Colors.Minutes;
                                                  r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                                  return r, g, b
                                             end,
                                             set = function( _, r, g, b, a )
                                                  r = r * 255; g = g * 255; b = b * 255;
                                                  newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                                  TitanTreasury.Colors.Minutes = newHex;
                                             end,
                                             hasAlpha = false,
                                        },
                                        secondscolor = {
                                             type = "color", order = 5,
                                             name = LB["TITAN_TREASURY_TOOLTIP_COLORS_SECONDS"], -- "Seconds Text Color",
                                             desc = function() return LB["TITAN_TREASURY_TOOLTIP_COLORS_SECONDS_DESC"]:format( TitanTreasury.Colors.Seconds ); end, -- "Lets you select the color ( %s ) for the word Seconds.",
                                             get = function()
                                                  local color = TitanTreasury.Colors.Seconds;
                                                  r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                                  return r, g, b
                                             end,
                                             set = function( _, r, g, b, a )
                                                  r = r * 255; g = g * 255; b = b * 255;
                                                  newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                                  TitanTreasury.Colors.Seconds = newHex;
                                             end,
                                             hasAlpha = false,
                                        },
                                        rNamecolor = {
                                             type = "color", order = 6,
                                             name = LB["TITAN_TREASURY_TOOLTIP_COLORS_REALM"], -- "Realm Name Color",
                                             desc = function() return LB["TITAN_TREASURY_TOOLTIP_COLORS_REALM_DESC"]:format( TitanTreasury.Colors.Realm ); end, -- "Lets you select the color ( %s ) for Realm name.",
                                             get = function()
                                                  local color = TitanTreasury.Colors.Realm;
                                                  r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                                  return r, g, b
                                             end,
                                             set = function( _, r, g, b, a )
                                                  r = r * 255; g = g * 255; b = b * 255;
                                                  newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                                  TitanTreasury.Colors.Realm = newHex;
                                             end,
                                             hasAlpha = false,
                                        },
                                   },
                              },
                         },
                    },
                    sessiontab = {
                         type = "group", order = 5,
                         name = LB["TITAN_TREASURY_CONFIG_GROUP_SESSION"], -- "Session Data",
                         desc = LB["TITAN_TREASURY_CONFIG_GROUP_SESSION_DESC"], -- "Settings for how session data is handled.",
                         args = {
                              sessdesc = {
                                   type = "description", order = 1, width = "full",
                                   name = LB["TITAN_TREASURY_CONFIG_SESSION_INFO"], -- "|cffff0000NOTE :|r Session data is reset every time you reload.",
                                   cmdHidden = true,
                                   hidden = false,
                              },
                              ShowSessionData = {
                                   type = "toggle", order = 2, width = "double",
                                   name = LB["TITAN_TREASURY_CONFIG_SESSION_DATA"], -- "Show Session Data",
                                   desc = LB["TITAN_TREASURY_CONFIG_SESSION_DATA_DESC"], -- "Show session data on tooltip",
                                   get = function() return TitanTreasury.Sessions.ShowSession; end,
                                   set = function( _, value ) TitanTreasury.Sessions.ShowSession = value; end,
                              },
                              ShowSessionHourlyData = {
                                   type = "toggle", order = 3, width = "double",
                                   name = LB["TITAN_TREASURY_CONFIG_SESSION_HOURLY"], -- "Show Hourly Session Data",
                                   desc = LB["TITAN_TREASURY_CONFIG_SESSION_HOURLY_DESC"], -- "Show hourly session data on tooltip",
                                   get = function() return TitanTreasury.Sessions.ShowHourly; end,
                                   set = function( _, value ) TitanTreasury.Sessions.ShowHourly = value; end,
                              },
                         },
                    },
               },
          },
          button = { -- Button Settings
               type = "group", order = 4,
               name = LB["TITAN_TREASURY_CONFIG_GROUP_BUTTON"], -- "Button",
               desc = LB["TITAN_TREASURY_CONFIG_GROUP_BUTTON_DESC"], -- "Changes the way the button is displayed.",
               args = {
                    displayType = {
                         type = "select", order = 1,
                         name = LB["TITAN_TREASURY_CONFIG_BUTTON_TYPE"], -- "Display Type",
                         desc = LB["TITAN_TREASURY_CONFIG_BUTTON_TYPE_DESC"], -- "The type information that will be shown on the button.",
                         get = function() return TitanTreasury.Button.Mode; end,
                         set = function( _, value )
                              TitanTreasury.Button.Mode = value;
                              TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                         end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanTreasuryStaticArrays.Modes, function( Key, Data )
                                        if ( Data.Value ~= TitanTreasury.Button.Mode ) then
                                             preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                        else
                                             preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                        end
                                   end
                              );
                              return preList;
                         end,
                    },
                    buttonType = {
                         type = "select", order = 2,
                         name = LB["TITAN_TREASURY_CONFIG_BUTTON_MONEY_TYPE"], -- "Money Type",
                         desc = LB["TITAN_TREASURY_CONFIG_BUTTON_MONEY_TYPE_DESC"], -- "This will make the money show in text or coin.",
                         get = function() return TitanTreasury.Button.Type; end,
                         set = function( _, value )
                              TitanTreasury.Button.Type = value;
                              TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                         end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanTreasuryStaticArrays.Types, function( Key, Data )
                                        if ( Data.Value ~= TitanTreasury.Button.Type ) then
                                             preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                        else
                                             preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                        end
                                   end
                              );
                              return preList;
                         end
                    },
                    buttonStyle = {
                         type = "select", order = 3,
                         name = LB["TITAN_TREASURY_CONFIG_BUTTON_MONEY_STYLE"], -- "Money Style",
                         desc = LB["TITAN_TREASURY_CONFIG_BUTTON_MONEY_STYLE_DESC"], -- "This will show the money in plain or colored text.",
                         get = function() return TitanTreasury.Button.Style; end,
                         set = function( _, value )
                              TitanTreasury.Button.Style = value;
                              TitanPanelButton_UpdateButton( LB["TITAN_TREASURY_CORE_ID"] );
                         end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanTreasuryStaticArrays.Styles, function( Key, Data )
                                        if ( Data.Value ~= TitanTreasury.Button.Style ) then
                                             preList[Data.Value] = ( "|cff19ff19%s|r" ):format( Data.Name );
                                        else
                                             preList[Data.Value] = ( "|cffffff9a%s|r" ):format( Data.Name );
                                        end
                                   end
                              );
                              return preList;
                         end
                    },
               },
          },
          colors = { -- Faction Colors
               type = "group", order = 5, childGroups = "tab",
               name = LB["TITAN_TREASURY_CONFIG_COLORS"], -- "Colors",
               desc = LB["TITAN_TREASURY_CONFIG_COLORS_DESC"], -- "Colors that will be used for faction reputation, tokens and professions progression.",
               args = {
                    cstandings = {
                         type = "group", order = 1,
                         name = LB["TITAN_TREASURY_CONFIG_STANDINGS"], -- "Standings",
                         args = {
                              colorone = {
                                   type = "color", order = 1, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 1, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 1, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[1];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[1] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colortwo = {
                                   type = "color", order = 2, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 2, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 2, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[2];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[2] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colorthree = {
                                   type = "color", order = 3, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 3, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 3, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[3];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[3] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colorfour = {
                                   type = "color", order = 4, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 4, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 4, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[4];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[4] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colorfive = {
                                   type = "color", order = 5, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 5, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 5, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[5];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[5] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colorsix = {
                                   type = "color", order = 6, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 6, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 6, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[6];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[6] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colorseven = {
                                   type = "color", order = 7, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 7, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 7, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[7];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[7] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              coloreight = {
                                   type = "color", order = 8, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCStanding", 8, "label" ); end, -- "%s",
                                   desc = function() return LB["TITAN_TREASURY_CONFIG_CSTANDING"]:format( TitanTreasury_ColorUtils( "GetCStanding", 8, "label" ) ) end, -- "This is the color used for %s standing level",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.rep[8];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.rep[8] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              demo = {
                                   type = "execute", order = 9,
                                   name = LB["TITAN_TREASURY_CONFIG_COLOR_DEMO"], -- "Color Demo",
                                   desc = LB["TITAN_TREASURY_CONFIG_COLOR_DEMO_DESC"], -- "Prints color demo to chat frame",
                                   func = function()
                                        local part1, part2, tLine = "", "", "";
                                        for i = 1, 8 do
                                             part1 = TitanTreasury_ColorUtils( "GetCDemo", i );
                                             part2 = TitanTreasury_ColorUtils( "GetCStanding", i, "label" );
                                             tLine = ( "%s - %s" ):format( part1, part2 );
                                             print( tLine );
                                        end
                                   end,
                              },
                         },
                    },
                    cgradient = { -- Gradient Colors
                         type = "group", order = 2,
                         name = LB["TITAN_TREASURY_CONFIG_GRADIENT"], -- "Gradient",
                         hidden = function()
                              if ( TitanTreasury.Professions.Colored == 2 and TitanTreasury.Tokens.Colored == 2 ) then return false; end
                              return true;
                         end,
                         args = {
                              colorone = {
                                   type = "color", order = 1, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 1, LB["TITAN_TREASURY_CONFIG_GRADIENT1"] ); end, -- "Gradient Color #1",
                                   desc = LB["TITAN_TREASURY_CONFIG_GRADIENT1_DESC"], -- "This will be the color the gradient will start from",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.progress[1];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.progress[1] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              colortwo = {
                                   type = "color", order = 2, width = "full",
                                   name = function() return TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, LB["TITAN_TREASURY_CONFIG_GRADIENT2"] ); end, -- "Gradient Color #2",
                                   desc = LB["TITAN_TREASURY_CONFIG_GRADIENT2_DESC"], -- "This will be the color the gradient will start from",
                                   get = function()
                                        local color = TitanTreasuryStaticArrays.Standings.base.progress[2];
                                        r, g, b = TitanTreasury_CoreUtils( "HexToDecP", color );
                                        return r, g, b
                                   end,
                                   set = function( _, r, g, b, a )
                                        r = r * 255; g = g * 255; b = b * 255;
                                        newHex = ( "%02x%02x%02x" ):format( r, g, b );
                                        TitanTreasuryStaticArrays.Standings.base.progress[2] = newHex;
                                        TitanTreasury_ColorUtils( "GenCGradient" );
                                   end,
                                   hasAlpha = false,
                              },
                              demo = {
                                   type = "execute", order = 3,
                                   name = LB["TITAN_TREASURY_CONFIG_COLOR_DEMO"], -- "Color Demo",
                                   desc = LB["TITAN_TREASURY_CONFIG_COLOR_DEMO_DESC"], -- "Prints color demo to chat frame",
                                   func = function() print( TitanTreasury_ColorUtils( "GetCDemo" ) ); end,
                              },
                         },
                    },
               },
          },
     },
};

-- **************************************************************************
-- NAME : TitanPanelTreasuryButton_OnLoad( self )
-- DESC : Registers the plugin upon it loading
-- tooltipTitle = LB["TITAN_TREASURY_TOOLTIP"],
-- **************************************************************************
function TitanPanelTreasuryButton_OnLoad( self )
     self.registry = {
          id = LB["TITAN_TREASURY_CORE_ID"],
          builtIn = false,
          category = "Information",
          version = TITAN_VERSION,
          menuText = LB["TITAN_TREASURY_MENU_TEXT"],
          tooltipTitle = LB["TITAN_TREASURY_TOOLTIP"],
          tooltipTextFunction = "TitanPanelTreasuryButton_GetTooltipText",
          buttonTextFunction = "TitanPanelTreasuryButton_GetButtonText",
          icon = LB["TITAN_TREASURY_CORE_ARTWORK"],
          iconWidth = 16,
          controlVariables = {
               ShowIcon = true,
               ShowLabelText = true,
          },
          savedVariables = {
	       ShowIcon = 1,
	       ShowLabelText = 1,
          },
     };
     self:RegisterEvent( "ADDON_LOADED" );
     self:RegisterEvent( "PLAYER_MONEY" );
     self:RegisterEvent( "PLAYER_ENTERING_WORLD" );
     self:RegisterEvent( "PLAYER_LEAVING_WORLD" );
     self:RegisterEvent( "COMBAT_TEXT_UPDATE" );
     self:RegisterEvent( "CHAT_MSG_SYSTEM" );
     self:RegisterEvent( "PLAYER_LEVEL_UP" );
     self:RegisterEvent( "TIME_PLAYED_MSG" );
     Config:RegisterOptionsTable( PlugInName, _G["TreasuryOpts"] );
end

function TitanPanelTreasuryButton_SetTooltipTitle()
     local TTFR = TitanTreasury_ColorUtils( "GetCFaction", TitanTreasuryProfile.Faction, ( "%s-%s" ):format( TitanTreasuryProfile.Faction, TitanTreasuryProfile.Realm ) );
     local ttUser = TitanTreasury_ColorUtils( "GetCClass", TitanTreasuryProfile.Class, TitanTreasuryProfile.Player );
     return LB["TITAN_TREASURY_TITLE"]:format( ttUser, TTFR );
end

-- **************************************************************************
-- NAME : SLASH_TITANPALS*
-- DESC : Slash command setup
-- **************************************************************************
SLASH_TitanTreasury1 = "/TitanTreasury";
SLASH_TitanTreasury2 = "/tt";
SlashCmdList["TitanTreasury"] = function( msg )
     TitanTreasury_CommandHandler( "TitanTreasury", msg );
end

-- **************************************************************************
-- NAME : TitanTreasury_ShowHelp( cmd )
-- DESC : Slash command help display
-- **************************************************************************
function TitanTreasury_ShowHelp( cmd )
     local CP = TitanTreasuryProfile;
     TitanTreasury_DebugUtils( "Info", "help", 1273, LB["TITAN_TREASURY_HELP_LINE1"] );
     TitanTreasury_DebugUtils( "Info", "help", 1274, LB["TITAN_TREASURY_HELP_LINE2"]:format( cmd ) );
     TitanTreasury_DebugUtils( "Info", "help", 1275, LB["TITAN_TREASURY_HELP_LINE3"]:format( cmd ) );
     TitanTreasury_DebugUtils( "Info", "help", 1276, LB["TITAN_TREASURY_HELP_LINE4"]:format( cmd ) );
     TitanTreasury_DebugUtils( "Info", "help", 1277, LB["TITAN_TREASURY_HELP_LINE5"]:format( cmd ) );
     local auth = TitanTreasury_CoreUtils( "Author", ( "%s@%s" ):format( CP.Player, CP.Realm ) )
     if ( auth ) then
          TitanTreasury_DebugUtils( "Info", "help", 1280, LB["TITAN_TREASURY_HELP_ADDAUTH"]:format( cmd ) );
          TitanTreasury_DebugUtils( "Info", "help", 1281, LB["TITAN_TREASURY_HELP_REMAUTH"]:format( cmd ) );
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_CommandHandler( slash, command )
-- DESC : Slash command handler
-- **************************************************************************
function TitanTreasury_CommandHandler( slash, command )
     local cmd1, arg1, arg2
     _, _, cmd1, arg1, arg2 = strfind( strlower( string.lower( command ) ), "(%w+)[ ]?(%w*)[ ]?([-%w]*)" );
     if ( not cmd1 ) then cmd1 = string.lower( command ); end
     if ( not arg1 ) then arg11 = ""; end
     if ( not arg2 ) then arg2 = ""; end
     if ( cmd1 == "" or cmd1 == "help" ) then
          TitanTreasury_ShowHelp( slash );
     elseif ( cmd1 == "options" or cmd1 == "config" ) then
          TitanTreasury_InitGUI();
      elseif ( cmd1 == "reset" ) then
          TitanTreasury_ResetUtils( true, cmd1 );
     elseif ( cmd1 == "checkdb" ) then
          TitanTreasury_ResetUtils( false, cmd1 );
     elseif ( cmd1 == "addauth" ) then
          TitanTreasury_CoreUtils( cmd1 );
     elseif ( cmd1 == "remauth" ) then
          TitanTreasury_CoreUtils( cmd1 );
     else
          TitanTreasury_DebugUtils( "error", "CommandHandler", 1308, LB["TITAN_TREASURY_HELP_ERROR"]:format( cmd1 ) );
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_BuildTokensPanel()
-- DESC : Build a tokens list for filtering in the Blizz Options Panel
-- **************************************************************************
function TitanTreasury_BuildTokensPanel()
     local Player = TitanTreasuryProfile
     local title = LB["TITAN_TREASURY_PANEL_TOKENS_TITLE"]:format( Player.Player );
     local tCount = C_CurrencyInfo.GetCurrencyListSize();
     local name, isHeader, isWatched, count, icon, nName
     local order = 1;
     TreasuryOpts.args.tooltip.args.currencytab.args.tokentab.args.AllowedTokens.name = ( "%s" ):format( TitanTreasury_ColorUtils( "GetCClass", TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Class, title ) );
     TreasuryOpts.args.tooltip.args.currencytab.args.tokentab.args.AllowedTokens.args = {}
     for i = 1, tCount do
          local name, isHeader, count, icon = TitanTreasury_convertTokens( C_CurrencyInfo.GetCurrencyListInfo( i ) );
          local currency = C_CurrencyInfo.GetCurrencyListInfo( i );
          if ( not isHeader ) then
               if ( TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Tokens[ name ] == nil ) then
                    TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Tokens[ name ] = true;
               end
               TreasuryOpts.args.tooltip.args.currencytab.args.tokentab.args.AllowedTokens.args[ name ] = {};
               local toggle = TreasuryOpts.args.tooltip.args.currencytab.args.tokentab.args.AllowedTokens.args[ name ];
               toggle.type = "toggle";
               toggle.order = order;
               toggle.width = "double";
               toggle.name = ( "%s ( %s )" ):format( name, count );
               toggle.desc = LB["TITAN_TREASURY_PANEL_TOKENS_COUNT"]:format( TitanTreasury_ColorUtils( "GetCText", "lime", count ) );
               toggle.image = icon;
               toggle.get = function() return TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Tokens[ name ]; end
               toggle.set = function( _, value ) TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Tokens[ name ] = value; end
               order = order + 1;
          end
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_BuildProfessionsPanel()
-- DESC : Build a professions list for filtering in the Blizz Options Panel
-- **************************************************************************
function TitanTreasury_BuildProfessionsPanel()
     local Player = TitanTreasuryProfile
     local tCount = C_CurrencyInfo.GetCurrencyListSize();
     local order = 1;
     local title = LB["TITAN_TREASURY_PANEL_PROFESSIONS_TITLE"]:format( Player.Player );
     local order, known = 1, {};
     known.prof1, known.prof2, known.archaeology, known.fishing, known.cooking, known.firstaid = GetProfessions();
     local count = TitanTreasury_CoreUtils( "getCount", known );
     TreasuryOpts.args.tooltip.args.currencytab.args.professiontab.args.AllowedProfessions.name = ( "%s" ):format( TitanTreasury_ColorUtils( "GetCClass", TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Class, title ) );
     TreasuryOpts.args.tooltip.args.currencytab.args.professiontab.args.AllowedProfessions.args = {};
     if ( count > 0 ) then
          for profession, Index in pairs( known ) do 
               local name, icon, rank, mrank = GetProfessionInfo( Index );
               local iSize = TitanTreasury.Professions.IconSize;
               if ( TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Professions[ name ] == nil ) then TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Professions[ name ] = true; end
               TreasuryOpts.args.tooltip.args.currencytab.args.professiontab.args.AllowedProfessions.args[ name ] = {};
               local toggle = TreasuryOpts.args.tooltip.args.currencytab.args.professiontab.args.AllowedProfessions.args[ name ];
               toggle.type = "toggle";
               toggle.order = order;
               toggle.width = "double";
               toggle.name = ( "%s ( %s / %s )" ):format( name, rank, mrank );
               toggle.desc = LB["TITAN_TREASURY_PANEL_PROFESSIONS_LEVEL"]:format( name, TitanTreasury_ColorUtils( "GetCText", "lime", rank ) );
               toggle.image = TitanTreasury_CoreUtils( "getImage", icon, iSize );
               toggle.get = function() return TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Professions[ name ]; end
               toggle.set = function( _, value ) TitanTreasuryCharacters[ Player.Realm ][ Player.Player ].Professions[ name ] = value; end
               order = order + 1;
          end
     else
          TreasuryOpts.args.tooltip.args.currencytab.args.professiontab.args.AllowedProfessions.args[ "ProfessionError" ] = {};
          local description = TreasuryOpts.args.tooltip.args.currencytab.args.professiontab.args.AllowedProfessions.args[ "ProfessionError" ];
          description.type = "description";
          description.order = 1;
          description.name = LB["TITAN_TREASURY_CONFIG_ERROR_PBUILD"];
          description.cmdHidden = true;
     end
end

-- **************************************************************************
-- NAME : TitanTreasury_convertTokens( data )
-- DESC : Messy work around for blizzard api change
-- **************************************************************************
function TitanTreasury_convertTokens( data )
     local name, isHeader, count, icon;
     name = data.name;
     isHeader = data.isHeader;
     count = data.quantity;
     icon = data.iconFileID;
     return name, isHeader, count, icon;
end
