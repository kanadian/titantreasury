-- **************************************************************************
-- * TitanTreasuryTooltip.lua
-- *
-- * By: KanadiaN
-- *       (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanTreasury", true );

-- ********************************************************************************************************
-- NAME : TitanPanelTreasuryButton_GetTooltipText()
-- DESC : Builds Tooltip for use with Titan Panel
-- ********************************************************************************************************
function TitanPanelTreasuryButton_GetTooltipText()
     local TP = TitanTreasuryProfile;
     local TC = TitanTreasuryCharacters[ TP.Realm ][ TP.Player ];
     local aData = TitanTreasuryCharacters[ TP.Realm ];
     local CL, name, isHeader, isWatched, count, icon, aLevel, mString, tIcon, pName, pCount;
     local rCount, rank, maxrank, mString, gName, v, cGradient, left, sTotal, dType, sColor, aStr;
     local tHourly, pTime, class, cCount, Guild, gName, fLine, nStanding, standing, barMin, barMax;
     local barValue, atWarWith, right, sMethod, percent, percent2;
     local gLine, tLine, mLine, pLine, rLine, image, cCheck = "", "", "", "", "", "", "";
     local ttGuild, ttCoin, ttToon, ttSess, ttMem, ttFactions, ls, rs = "", "", "", "", "", "", "", "";
     local level, gold, cGold, cAmount, thisWeek, weeklyMax, totalMax, maximum, rMoney, aMoney = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
     local hasWeeklyLimit, currentWeeklyAmount = 0, 0;
     local known, mTable, dTable, nTable, cTable, v = {}, {}, {}, {}, {}, {};
     local isKey = {
          ["shift"] = IsShiftKeyDown(),
          ["control"] = IsControlKeyDown(),
          ["alt"] = IsAltKeyDown(),
     };
     local total = {
          ["Horde"] = 0,
          ["Alliance"] = 0,
          ["Neutral"] = 0,
     };
     local sType = "Faction"
     if ( TP.Faction == "Alliance" ) then
          sMethod = "Ascending";
     elseif ( TP.Faction == "Horde" ) then
          sMethod = "Descending";
     end
     if ( TitanTreasury.General.dColor == "CLASS" ) then
          CL = TitanTreasury_ColorUtils( "GetCClass", TC.Class, LB[ ( "TITAN_TREASURY_TOOLTIP_LINE_%d_%d" ):format( TitanTreasury.General.dStyle, TitanTreasury.General.dType ) ] );
     elseif ( TitanTreasury.General.dColor ~= "CLASS" ) then
          CL = TitanTreasury_ColorUtils( "GetCText", TitanTreasury.General.dColor, LB[ ( "TITAN_TREASURY_TOOLTIP_LINE_%d_%d" ):format( TitanTreasury.General.dStyle, TitanTreasury.General.dType ) ] );
     end
     if ( not TitanTreasury.General.ttHelp or TitanTreasury.General.ttHelp == nil ) then
          local ShowShift, ShowAlt = TitanTreasury_CoreUtils( "convertBool", TitanTreasury.General.ShowShift ), TitanTreasury_CoreUtils( "convertBool", TitanTreasury.General.ShowAlt );
          finTooltip = LB["TITAN_TREASURY_TOOLTIP_HELP"]:format( ShowShift, ShowAlt );
     elseif ( TitanTreasury.General.ShowShift and isKey["shift"]  ) then
     -- ========================
     -- 
     -- Realm & Account Summary ( Secondary Menu )
     -- 
     -- ========================
          mTable = TitanTreasury_CoreUtils( "getMoneyTable", aData, TP, true );
          ttFactions = "\n";
          ttFactions = ( "%s%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "sgray", LB["TITAN_TREASURY_TOOLTIP_SUMMARY"]:format( TP.Realm ) ) );
          ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
          table.foreach( mTable, function( Key, Data )
                    image = TitanTreasury_CoreUtils( "getBadge", Data.Faction, Data.Faction );
                    aLevel = ( "  (%s) %s %s" ):format( TitanTreasury_ColorUtils( "GetCText", "orange", ( "%02d" ):format( Data.Level ) ), image, TitanTreasury_ColorUtils( "GetCClass", Data.Class, Data.Name ) );
                    mString = TitanTreasury_CoreUtils( "GetMoney", Data.Gold, "Tooltip" );
                    ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, aLevel, mString );
                    rMoney = rMoney + Data.Gold;
               end
          )
          ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
          ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "lime", LB["TITAN_TREASURY_TOOLTIP_GRAND_TOTAL"]:format( LB["TITAN_TREASURY_TOOLTIP_REALM"] ) ), TitanTreasury_CoreUtils( "GetMoney", rMoney, "Tooltip" ) );

          nTable = TitanTreasury_CoreUtils( "getAccountTable", TitanTreasuryCharacters, TP, true );
          ttFactions = ( "%s\n\n" ):format( ttFactions );
          ttFactions = ( "%s%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "sgray", LB["TITAN_TREASURY_TOOLTIP_SUMMARY"]:format( LB["TITAN_TREASURY_TOOLTIP_ACCOUNT"] ) ) );
          ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
          aMoney = 0;

          table.foreach( nTable, function( Key, Data )
                    aLevel = ( "  |cff%s%s" ):format( TitanTreasury.Colors.Realm, Data.Name );
                    mString = TitanTreasury_CoreUtils( "GetMoney", Data.Gold, "Tooltip" );
                    ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, aLevel, mString );
                    aMoney = aMoney + Data.Gold;
               end
          )
          ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
          ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "lime", LB["TITAN_TREASURY_TOOLTIP_GRAND_TOTAL"]:format( LB["TITAN_TREASURY_TOOLTIP_ACCOUNT"] ) ), TitanTreasury_CoreUtils( "GetMoney", aMoney, "Tooltip" ) );
          finTooltip = ( "%s" ):format( ttFactions );
     elseif ( TitanTreasury.General.ShowAlt and isKey["alt"] ) then
          if ( TitanTreasury.General.levelSummary ) then
               ttFactions = ( "%s\n\n" ):format( ttFactions );
               ttFactions = ( "%s%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "sgray", LB["TITAN_TREASURY_TOOLTIP_SUMMARY"]:format( TitanTreasury_ColorUtils( "GetCClass", TC.Class, TP.Player ) ) ) );
               ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
               ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, LB["TITAN_TREASURY_TOOLTIP_PLAYED_LEVEL"]:format( TitanTreasury.Colors.Realm ), TitanTreasury_CoreUtils( "formatPlayed", TC.Played.level ) )
          end
          if ( TitanTreasury.General.curRealm ) then
               local realm = TitanTreasuryStaticArrays.realms[ TitanTreasury.General.Watched ];
               aStr = 0;
               ttFactions = ( "%s\n\n" ):format( ttFactions );
               ttFactions = ( "%s%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "sgray", LB["TITAN_TREASURY_TOOLTIP_SUMMARY"]:format( realm ) ) );
               ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
               cTable = TitanTreasury_CoreUtils( "getPlayedTable", "watched", realm );
               table.foreach( cTable, function( Key, Data )
                         aStr = aStr + Data;
                         ttFactions = ( "%s\n|cff%s%s\t%s" ):format( ttFactions, TitanTreasury.Colors.Realm, Key, TitanTreasury_CoreUtils( "formatPlayed", Data ) );
                    end
               )
               ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
               ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "lime", LB["TITAN_TREASURY_TOOLTIP_PLAYED_TOTAL"] ), TitanTreasury_CoreUtils( "formatPlayed", aStr ) );
          end
          if ( TitanTreasury.General.Summary ) then
               aStr = 0;
               ttFactions = ( "%s\n\n" ):format( ttFactions );
               ttFactions = ( "%s%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "sgray", LB["TITAN_TREASURY_TOOLTIP_SUMMARY"]:format( LB["TITAN_TREASURY_TOOLTIP_ACCOUNT"] ) ) );
               ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
               nTable = TitanTreasury_CoreUtils( "getPlayedTable", "summary" );
               table.foreach( nTable, function( Key, Data )
                         aStr = aStr + Data
                         ttFactions = ( "%s\n|cff%s%s\t%s" ):format( ttFactions, TitanTreasury.Colors.Realm, Key, TitanTreasury_CoreUtils( "formatPlayed", Data ) );
                    end
               )
               ttFactions = ( "%s\n%s" ):format( ttFactions, CL );
               ttFactions = ( "%s\n%s\t%s" ):format( ttFactions, TitanTreasury_ColorUtils( "GetCText", "lime", LB["TITAN_TREASURY_TOOLTIP_PLAYED_TOTAL"] ), TitanTreasury_CoreUtils( "formatPlayed", aStr ) );
          end
          finTooltip = ( "%s" ):format( ttFactions );
     else
          -- ========================
          --
          -- Guild Reputation Display
          --
          -- ========================
          if ( IsInGuild() and TitanTreasury.General.ShowGuildRep ) then
               ttGuild = "\n";
               Guild = GetGuildInfo( "player" );
               name, _, standing, barMin, barMax, barValue, atWarWith, _, isHeader, _, _ = GetFactionInfo( 2 );
               if ( not isHeader ) then
                    if ( name == Guild ) then
                         percent = math.floor( barValue / barMax * 100 );
                         if ( not v ) then v = {}; end
                         v = {
                              [1] = ( barValue - barMin ),
                              [2] = ( barMax - barMin ),
                              [3] = ( barMax - barMin ) - ( barValue - barMin ),
                              [4] = percent,
                              [5] = ( " %d%%" ):format( percent ),
                         };
                         -- Pick and return gradient by %
                         if ( TitanTreasury.Guild.Style == 1 ) then
                              cGradient = "%s";
                         elseif ( TitanTreasury.Guild.Style == 2 ) then
                              cGradient = TitanTreasury_ColorUtils( "GetCGradient", "rep", standing, v[4] );
                         end
                         if ( TitanTreasury.Guild.ShowGain ) then
                              if ( TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ].sRep > 0 ) then
                                   gLine = cGradient:format( TitanTreasuryCharacters[ TitanTreasuryProfile.Realm ][ TitanTreasuryProfile.Player ].sRep );
                              else
                                   gLine = "";
                              end
                         end
                         if ( TitanTreasury.Guild.ShowPercent ) then
                              pLine = cGradient:format( v[5] );
                         end
                         if ( TitanTreasury.Guild.ShowRaw ) then
                              rLine = ( " ( %s / %s )" ):format( cGradient:format( v[1] ), TitanTreasury_ColorUtils( "GetCStanding", standing, v[2] ) );
                         end
                         if ( TitanTreasury.Guild.ShowRemain ) then
                              mLine = ( " %s" ):format( cGradient:format( v[3] ) );
                         end
                         if ( TitanTreasury.Guild.ShowRepText ) then
                              tLine = ( " %s" ):format( TitanTreasury_ColorUtils( "GetCStanding", standing, "label" ) );
                         end
                    end
               end
               gName = TitanTreasury_ColorUtils( "GetCText", "lime", ( "< %s >" ):format( Guild ) );
               ttGuild = ( "%s%s\t%s" ):format( ttGuild, gName, TitanTreasury_ColorUtils( "GetCFaction", TP.Faction, ( "%s - %s" ):format( TP.Faction, TP.Realm ) ) );
               ttGuild = ( "%s\n%s" ):format( ttGuild, CL );
               if ( TitanTreasury.Guild.ShowPercent and TitanTreasury.Guild.ShowRepText ) then
                    ls = ( "%s %s%s" ):format( pLine, TitanTreasury_ColorUtils( "GetCClass", TC.Class, LB["TITAN_TREASURY_TOOLTIP_OF"] ), tLine );
               else
                    ls = ( "%s%s" ):format( pLine, tLine );
               end
               rs = ( "%s%s%s" ):format( gLine, rLine, mLine );
               ttGuild = ( "%s\n%s\t%s" ):format( ttGuild, ls, rs );
               ttCoin = "\n";
          end
          -- =================================
          --
          -- Character / Alts Currency Display
          --
          -- =================================
          local mTable = TitanTreasury_CoreUtils( "getMoneyTable", aData, TP );
          table.foreach( mTable, function( Key, Data )
                    if ( total[ Data.Faction ] == nil ) then total[ Data.Faction ] = 0; end
                    if ( Data.Name == TP.Player ) then
                         cGold = Data.rGold;
                         total[ Data.Faction ] = total[ Data.Faction ] + Data.rGold;
                    else
                         cGold = Data.Gold;
                         total[ Data.Faction ] = total[ Data.Faction ] + Data.Gold;
                    end
                    image = TitanTreasury_CoreUtils( "getBadge", Data.Badge, Data.Faction );
                    aLevel = ( "  (%s) %s %s" ):format( TitanTreasury_ColorUtils( "GetCText", "orange", ( "%02d" ):format( Data.Level ) ), image, TitanTreasury_ColorUtils( "GetCClass", Data.Class, Data.Name ) );
                    mString = TitanTreasury_CoreUtils( "GetMoney", cGold, "Tooltip" );
                    ttCoin = ( "%s\n%s\t%s" ):format( ttCoin, aLevel, mString );
               end
          )
          ttCoin = ( "%s\n%s" ):format( ttCoin, CL );
          if ( TitanTreasury.General.ShowAll ) then
               table.insert( dTable, { Faction = "Horde", Money = TitanTreasury_CoreUtils( "GetMoney", total[ "Horde" ], "Tooltip" ), Image = TitanTreasury_CoreUtils( "getBadge", "Horde", "Horde" ), Text = TitanTreasury_ColorUtils( "GetCText", "lime", LB["TITAN_TREASURY_TOOLTIP_HTOTAL"] ) } );
               table.insert( dTable, { Faction = "Alliance", Money = TitanTreasury_CoreUtils( "GetMoney", total[ "Alliance" ], "Tooltip" ), Image = TitanTreasury_CoreUtils( "getBadge", "Alliance", "Alliance" ), Text = TitanTreasury_ColorUtils( "GetCText", "lime", LB["TITAN_TREASURY_TOOLTIP_ATOTAL"] ) } );
               dTable = TitanTreasury_CoreUtils( "sortTable", dTable, sType, sMethod );
               table.foreach( dTable, function( Index, Data )
                         ttCoin = ( "%s\n%s\t%s" ):format( ttCoin, Data.Text, Data.Money );
                    end
               )
          else
               ttCoin = ( "%s\n%s\t%s" ):format( ttCoin, TitanTreasury_ColorUtils( "GetCText", "lime", LB[ "TITAN_TREASURY_TOOLTIP_TOTAL" ] ), TitanTreasury_CoreUtils( "GetMoney", total[ TP.Faction ], "Tooltip" ) );
          end
          -- ========================
          --
          -- Toon Information Display
          --
          -- ========================
          if ( TitanTreasury.General.ShowProfs or TitanTreasury.General.ShowTokens ) then
               ttToon = "\n";
               ttToon = ( "%s\n%s" ):format( ttToon, TitanTreasury_ColorUtils( "GetCClass", TC.Class, LB[ "TITAN_TREASURY_TOOLTIP_TOON" ]:format( TP.Player ) ) );
               ttToon = ( "%s\n%s" ):format( ttToon, CL );
               -- Profession Info Display
               -- =======================
               if ( TitanTreasury.General.ShowProfs ) then
                    known.prof1, known.prof2, known.archaeology, known.fishing, known.cooking, known.firstaid = GetProfessions();
                    table.foreach( known, function( profession, index )
                              name, icon, rank, rmax = GetProfessionInfo( index );
                              if ( TitanTreasuryCharacters[ TP.Realm ][ TP.Player ].Professions[ name ] ) then
                                   percent = math.floor( ( rank / rmax ) * 100 );
                                   -- cSet = TitanTreasury_CoreUtils( "getTradeSkillIndex", rmax );
                                   if ( TitanTreasury.Professions.Icon ) then
                                        if ( TitanTreasury.Professions.Name ) then
                                             if ( TitanTreasury.Professions.Colored == 1 ) then
                                                  pName = name;
                                                  if ( TitanTreasury.Professions.ShowMax ) then
                                                       rCount = ( "%s / %s" ):format( rank, rmax );
                                                  else
                                                       rCount = rank;
                                                  end
                                             elseif ( TitanTreasury.Professions.Colored == 2 ) then
                                                  pName = TitanTreasury_ColorUtils( "GetCText", "violet", name );
                                                  if ( TitanTreasury.Professions.ShowMax ) then
                                                       rCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, rank ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, rmax ) );
                                                  else
                                                       rCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, rank );
                                                  end
                                             end
                                             tIcon = ( "  |T%s:%s|t %s" ):format( icon, TitanTreasury.Professions.IconSize, pName );
                                        elseif ( not TitanTreasury.Professions.Name ) then
                                             if ( TitanTreasury.Professions.Colored == 1 ) then
                                                  if ( TitanTreasury.Professions.ShowMax ) then
                                                       rCount = ( "%s / %s" ):format( rank, rmax );
                                                  else
                                                       rCount = rank;
                                                  end
                                             elseif ( TitanTreasury.Professions.Colored == 2 ) then
                                                  if ( TitanTreasury.Professions.ShowMax ) then
                                                       rCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, rank ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, rmax ) );
                                                  else
                                                       rCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, rank );
                                                  end
                                             end
                                             tIcon = ( "  |T%s:%s|t " ):format( icon, TitanTreasury.Professions.IconSize );
                                        end
                                        ttToon = ( "%s\n%s\t%s" ):format( ttToon, tIcon, rCount );
                                   elseif ( not TitanTreasury.Professions.Icon ) then
                                        if ( TitanTreasury.Professions.Colored == 1 ) then
                                             pName = name;
                                             if ( TitanTreasury.Professions.ShowMax ) then
                                                  rCount = ( "%s / %s" ):format( rank, rmax );
                                             else
                                                  rCount = rank
                                             end
                                        elseif ( TitanTreasury.Professions.Colored == 2 ) then
                                             pName = TitanTreasury_ColorUtils( "GetCText", "sgray", name );
                                             if ( TitanTreasury.Professions.ShowMax ) then
                                                  rCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, rank ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, rmax ) );
                                             else
                                                  rCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, rank );
                                             end
                                        end
                                        ttToon = ( "%s\n%s\t%s" ):format( ttToon, pName, rCount );
                                   end
                              end
                         end
                    )
               end
               -- Token Info Display
               -- ==================
               if ( TitanTreasury.General.ShowTokens ) then
                    cCount = C_CurrencyInfo.GetCurrencyListSize();
                    for i = 1, cCount do
                         -- name, isHeader, _, _, _, count, icon, maximum, hasWeeklyLimit, currentWeeklyAmount = C_CurrencyInfo.GetCurrencyListInfo( i );
                         name, isHeader, count, icon = TitanTreasury_convertTokens( C_CurrencyInfo.GetCurrencyListInfo( i ) );
	                 if ( not isHeader ) then
                              if ( TitanTreasuryCharacters[ TP.Realm ][ TP.Player ].Tokens[ name ] ) then
                                   if ( maximum and strlen( maximum ) >= 6 and tonumber( strsub( maximum, 5 ) ) == 99 ) then maximum = tonumber( strsub( maximum, 1, 4 ) ); end
                                   cCheck = TitanTreasuryStaticArrays.cID[ name ];
                                   if ( cCheck ) then
                                        _, cAmount, _, thisWeek, weeklyMax, totalMax = C_CurrencyInfo.GetCurrencyInfo( cCheck );
                                        weeklyMax, totalMax = strsub( weeklyMax, 1, 4 ), strsub( totalMax, 1, 4 );
                                        percent2 = math.floor( ( thisWeek / weeklyMax ) * 100 );
                                        if ( TitanTreasury.Tokens.Colored == 2 ) then
                                             thisWeek = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent2, tonumber( thisWeek ) )
                                             weeklyMax = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, tonumber( weeklyMax ) );
                                        end
                                   end
                                   if ( maximum > 0 ) then
                                        percent = math.floor( ( count / maximum ) * 100 );
                                   else 
                                        percent = 100;
                                   end
                                   if ( TitanTreasury.Tokens.Icon ) then
                                        if ( TitanTreasury.Tokens.Name ) then
                                             if ( TitanTreasury.Tokens.Colored == 1 ) then
                                                  pName = name;
                                                  if ( TitanTreasury.Tokens.ShowMax ) then
                                                       if ( hasWeeklyLimit and maximum > 0 ) then
                                                            pCount = ( "%s / %s - %s / %s" ):format( thisWeek, weeklyMax, count, maximum );
                                                       elseif ( not hasWeeklyLimit and maximum > 0 ) then
                                                            pCount = ( "%s / %s" ):format( count, maximum );
                                                       else
                                                            pCount = count;
                                                       end
                                                  else
                                                       pCount = count ;
                                                  end
                                             elseif ( TitanTreasury.Tokens.Colored == 2 ) then
                                                  pName = TitanTreasury_ColorUtils( "GetCText", "sgray", name );
                                                  if ( TitanTreasury.Tokens.ShowMax ) then
                                                       if ( hasWeeklyLimit and maximum > 0 ) then
                                                            pCount = ( "%s |cffffffff/|r %s |cffffffff-|r %s |cffffffff/|r %s" ):format( thisWeek, weeklyMax, TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, maximum ) );
                                                       elseif ( not hasWeeklyLimit and maximum > 0 ) then
                                                            if ( count == maximum ) then
                                                                 pCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCText", "red", count ), TitanTreasury_ColorUtils( "GetCText", "red", maximum ) ); 
                                                            else
                                                                 pCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, maximum ) );
                                                            end
                                                       else
                                                            pCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count );
                                                       end
                                                  else
                                                       pCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count );
                                                  end
                                             end
                                             tIcon = ( "  |T%s:%s|t %s" ):format( icon, TitanTreasury.Tokens.IconSize, pName );
                                             ttToon = ( "%s\n%s\t%s" ):format( ttToon, tIcon, pCount );
                                        elseif ( not TitanTreasury.Tokens.Name ) then
                                             if ( TitanTreasury.Tokens.Colored == 1 ) then
                                                  if ( TitanTreasury.Tokens.ShowMax ) then
                                                       if ( hasWeeklyLimit and maximum > 0 ) then
                                                            pCount = ( "%s / %s - %s / %s" ):format( thisWeek, weeklyMax, count, maximum );
                                                       elseif ( not hasWeeklyLimit and maximum > 0 ) then
                                                            pCount = ( "%s / %s" ):format( count, maximum );
                                                       else
                                                            pCount = count;
                                                       end
                                                  else
                                                       pCount = count ;
                                                  end
                                             elseif ( TitanTreasury.Tokens.Colored == 2 ) then
                                                  if ( TitanTreasury.Tokens.ShowMax ) then
                                                       if ( hasWeeklyLimit and maximum > 0 ) then
                                                            pCount = ( "%s |cffffffff/|r %s |cffffffff-|r %s |cffffffff/|r %s" ):format( thisWeek, weeklyMax, TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, maximum ) );
                                                       elseif ( not hasWeeklyLimit and maximum > 0 ) then
                                                            if ( count == maximum ) then
                                                                 pCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCText", "red", count ), TitanTreasury_ColorUtils( "GetCText", "red", maximum ) ); 
                                                            else
                                                                 pCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, maximum ) );
                                                            end
                                                       else
                                                            pCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count );
                                                       end
                                                  else
                                                       pCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count );
                                                  end
                                             end
                                             tIcon = ( "  |T%s:%s|t " ):format( icon, TitanTreasury.Tokens.IconSize );
                                             ttToon = ( "%s\n%s\t%s" ):format( ttToon, tIcon, pCount );
                                        end
                                   elseif ( not TitanTreasury.Tokens.Icon ) then
                                        if ( TitanTreasury.Tokens.Colored == 1 ) then
                                             pName = name;
                                             if ( TitanTreasury.Tokens.ShowMax ) then
                                                  if ( hasWeeklyLimit and maximum > 0 ) then
                                                       pCount = ( "%s / %s - %s / %s" ):format( thisWeek, weeklyMax, count, maximum );
                                                  elseif ( not hasWeeklyLimit and maximum > 0 ) then
                                                       pCount = ( "%s / %s" ):format( count, maximum );
                                                  else
                                                       pCount = count;
                                                  end
                                             else
                                                  pCount = count ;
                                             end
                                             -- ttToon = ( "%s\n%s\t%s" ):format( ttToon, name, count );
                                        elseif ( TitanTreasury.Tokens.Colored == 2 ) then
                                             pName = TitanTreasury_ColorUtils( "GetCText", "sgray", name );
                                             if ( TitanTreasury.Tokens.ShowMax ) then
                                                  if ( hasWeeklyLimit and maximum > 0 ) then
                                                       pCount = ( "%s |cffffffff/|r %s |cffffffff-|r %s |cffffffff/|r %s" ):format(  thisWeek, weeklyMax, TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, maximum ) );
                                                  elseif ( not hasWeeklyLimit and maximum > 0 ) then
                                                       if ( count == maximum ) then
                                                            pCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCText", "red", count ), TitanTreasury_ColorUtils( "GetCText", "red", maximum ) ); 
                                                       else
                                                            pCount = ( "%s |cffffffff/|r %s" ):format( TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count ), TitanTreasury_ColorUtils( "GetCGradient", "progress", "", 100, maximum ) );
                                                       end
                                                  else
                                                       pCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count );
                                                  end
                                             else
                                                  pCount = TitanTreasury_ColorUtils( "GetCGradient", "progress", "", percent, count );
                                             end
                                        end
                                        ttToon = ( "%s\n%s\t%s" ):format( ttToon, pName, pCount );
                                   end
                              end
                         end
                    end
               end
          end
          -- ====================
          --
          -- Session Info Display
          --
          -- ====================
          if ( TitanTreasury.Sessions.ShowSession ) then
               ttSess = "\n";
               sTotal = GetMoney() - TC.Gold;
               if ( sTotal >= 0 ) then
                    sColor = "green"; dType = "TRUE";
               elseif ( sTotal < 0 ) then
                    sTotal = math.abs( sTotal );
                    sColor = "red"; dType = "FALSE";
               end
               ttSess = ( "%s\n%s" ):format( ttSess, TitanTreasury_ColorUtils( "GetCText", "sgray", LB[ "TITAN_TREASURY_TOOLTIP_SESSION" ] ) ); -- Session Statistics
               ttSess = ( "%s\n%s" ):format( ttSess, CL );
               ttSess = ( "%s\n%s\t%s" ):format( ttSess, TitanTreasury_ColorUtils( "GetCText", "lime", LB[ "TITAN_TREASURY_TOOLTIP_STARTING" ] ), TitanTreasury_CoreUtils( "GetMoney", TC.Gold, "Tooltip" ) ); -- Starting Gold
               pTime = GetTime() - TC.Session;
               tHourly = math.floor( sTotal / pTime * 3600 );
               if ( sTotal > 0 ) then
                    ttSess = ( "%s\n%s\t%s" ):format( ttSess, TitanTreasury_ColorUtils( "GetCText", sColor, LB[ ( "TITAN_TREASURY_SESSION_%s" ):format( dType ) ] ), TitanTreasury_CoreUtils( "GetMoney", sTotal, "Tooltip" ) );
               end
               if ( TitanTreasury.Sessions.ShowHourly and tHourly > 0 ) then
                    ttSess = ( "%s\n%s\t%s" ):format( ttSess, TitanTreasury_ColorUtils( "GetCText", sColor, LB[ ( "TITAN_TREASURY_SESSION_HOURLY_%s" ):format( dType ) ] ), TitanTreasury_CoreUtils( "GetMoney", tHourly, "Tooltip" ) );
               end
          end
          finTooltip = ( "%s%s%s%s" ):format( ttGuild, ttCoin, ttToon, ttSess );
     end
     -- ==================================================
     --
     -- Addon Memory Info Display
     --
     -- We will hide this if the tooltip help has not been
     -- viewed
     --
     -- ==================================================
     if ( TitanTreasury.General.ShowMem and TitanTreasury.General.ttHelp ) then
          ttMem = "\n";
          ttMem = ( "%s\n%s" ):format( ttMem, TitanTreasury_ColorUtils( "GetCText", "sgray", LB["TITAN_TREASURY_TOOLTIP_MEM"] ) );
          ttMem = ( "%s\n%s" ):format( ttMem, TitanTreasury_ColorUtils( "GetCText", "white", CL ) );
          ttMem = ( "%s\n%s\t%s" ):format( ttMem, LB["TITAN_TREASURY_TOOLTIP_MEMTEXT"], TitanTreasury_ColorUtils( "GetCText", "lime", ( "%skb" ):format( floor( GetAddOnMemoryUsage( "TitanTreasury" ) ) ) ) );
          finTooltip = ( "%s%s" ):format( finTooltip, ttMem );
     end
     -- Building the finished tooltip
     -- =========================
     if ( not TitanTreasury.General.ttHelp or TitanTreasury.General.ttHelp == nil ) then TitanTreasury.General.ttHelp = true; end
     return finTooltip;
end
